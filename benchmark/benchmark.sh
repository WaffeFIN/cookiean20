#
# Remember to allow access to flamegraph with:
# /proc/sys/kernel/kptr_restrict = 0
# /proc/sys/kernel/perf_event_parannoid = 0
#
# Reboot afterwards for security
cargo flamegraph --bin=baker_cli -- benchmark.qki