# Cookiean20

Language tutorial and documentation in the [wiki](https://bitbucket.org/WaffeFIN/cookiean20/wiki/Home).

## Install and run

Install Rust and Cargo, clone this repo, move to the repo root directory and then simply run `cargo run -h` for instructions.

## Notes

Language attempt #3. Learning Rust at the same time.

Update 2020.10.28: [Dev blog](https://waltergronholm.wixsite.com/cookiean)

Update 2020.10.6: After a long hiatus, the language is finally taking shape. Going to blog the progress somewhere else, TBA.

Update 2019.6.4: Successfully tested row parser after changing from Scala to Rust.

Update 2019.5.10: Earliest working WaffeStackLanguage (WSL) sample. Now considering to move to a more suitable programming language, maybe trying Rust?