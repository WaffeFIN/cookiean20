# TODO for next release

```does return``` and ```do return```

Colon is a marker to create a new scope/object
- the code
  ```
  displayed twice:
    x is 1
    y is 2
  ```
  is the same as
  ```
  _ does:
    return:
      x is 1
      y is 2
  
  _ displayed twice
  ```
- the code
  ```
  last name of Waffe is "Grönholm"
  name of Waffe is:
    first is "Walter"
  ```
  is the same as
  ```
  first name of Waffe is "Walter" (note: no 'last')
  ```

Should ```does twice: ...``` work like ```do twice: ...```?
- First feeling is yes. Unintuitive otherwise
- ```does return result of:``` could even be allowed

Make more tests for unwanted field/variable leakage

Add list to standard library

Pairing heap done with Cookiean

Errors could add a Return to the instruction stack?

Fix return:
- test_interpret_return_with_nested_1_plain
- test_interpret_return_with_nested_1_if_yes
- test_interpret_return_with_nested_1_if_no

List iteration.
- How to iterate procedures? OutOfCall --> redo procedure for next item.
- ```each person from person list is displayed```
- ```each person in person list is displayed```
- ```each of person list is displayed```
- ```each person from person table is converted``` *from* means the original values are not modified
- ```each person in person table is converted``` *in* modifies the original values

Fix left side comma:
- test_interpret_comma_in_left_side

Fix interpolated strings:
- test_interpret_interpolated_string_if_1
- test_interpret_interpolated_string_if_2
- test_interpret_interpolated_string_as_key

Basic fuzz testing

Sandbox website

Spektraklet

# TODO for future releases

Fix multi word method binding. Add analyzing step. Fix:
- test_interpret_binding_1a
- test_interpret_binding_1b
- test_interpret_binding_1c

Error debugging tool

Error stack trace

```why```, ```what``` and ```where``` debugging statements