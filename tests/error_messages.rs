mod common;
use common::assert_compile_error;

/**
 * This file contains tests asserting correctness and understandability of compile-time errors
 * 
 * Note: these tests are better suited as unit tests
 */

#[test]
fn test_broken_string() {
	assert_compile_error("\"this is a broken string", 1, vec!["rror at row 1", "tring is missing", "end", "quote"]);
}

#[test]
fn test_broken_comment() {
	assert_compile_error("(", 1, vec!["rror at row 1", "issing 1 closing parenthes", ")"]);
}

#[test]
fn test_broken_rows_parameters() {
	assert_compile_error("
x does asd [y]
y does [z] asd
[x] f [y] does [z]
[x] [y] f does z
f [y] [z] does asd
[x] f [y] does [z]
[x] [y] f does z
f [y] asd [z] does dsa
f [y] [z] asd does dsa
", 20, vec![
	"rror at row 2", "nexpected bracket",
	"rror at row 3", "nexpected bracket",
	"rror at row 4", "nexpected bracket",
	"rror at row 5",
	"rror at row 6", "nexpected bracket",
	"rror at row 7", "nexpected bracket",
	"rror at row 8",
	"rror at row 9",
	"rror at row 10", "nexpected bracket"
]);
}

#[test]
fn test_broken_indentation() {
	assert_compile_error("
x	is y
	x is:	y
", 4, vec![""]);
}

#[test]
fn test_broken_rows_verbs() {
	assert_compile_error("
x is is y
x is y is
do x does y
", 3, vec!["nexpected verb", "nexpected verb", "nexpected verb"]);
}