mod common;
use common::assert_output;

/**
 * Tests how methods are bound
 */

#[test]
fn test_binding_1a() {
	assert_output("
Waffe is:
	birth year is 1995
	year is 2022
	age of current year [me] is year of me - birth year of me

this be long name words words words is current year Waffe

age of this be long name words words words displayed
", "nothing\n") // TODO: how should this be handled?
}

#[test]
fn test_binding_1b() {
	assert_output("
Waffe is:
	birth year is 1995
	year is 2022
	age of current year [me] is year of me - birth year of me

current_year Waffe is current year Waffe

age of current_year Waffe displayed
", "27\n") // TODO: how should this be handled?
}

#[test]
fn test_binding_1c() {
	assert_output("
Waffe is:
	birth year is 1995
	year is 2022
	age of current year [me] is year of me - birth year of me

year_Waffe is year Waffe

age of current year_Waffe displayed
", "nothing\n") // TODO: how should this be handled?
}