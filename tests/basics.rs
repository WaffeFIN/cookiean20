mod common;
use common::{assert_io, assert_output};

/**
 * This file contains basic tests that should remain pretty much as is, such as:
 * - Basic IO: 'displayed' and 'inputted'
 * - assignments
 * - Basic math and logic
 * - getter and function calls
 * - Scoping
 * - control flow: 'if' and 'return'
 * 
 * If these tests pass, the user should (theoretically) be able to code almost anything.
 */

#[test]
fn test_empty_1() {
	assert_output("", "");
}

#[test]
fn test_empty_2() {
	assert_output("\n", "");
}

#[test]
fn test_basic_is() {
	assert_output("
(commenting something)
\"0\" displayed
0 incremented displayed
2 is displayed
2 incremented is displayed
x y is 1 + 3
y y is 9 - 4
x y is displayed
y y is displayed
104 modulo 7 displayed
21 / 3 is displayed
2 * 4 is displayed
", "0\n1\n2\n3\n4\n5\n6\n7\n8\n");
}

#[test]
fn test_integer_arithmetic() {
	assert_output("
123 + 45 is displayed
234 * 56 is displayed
345 - 67 is displayed
456 / 76 is displayed
567 modulo 89 is displayed
", "168\n13104\n278\n6\n33\n");
}

#[test]
fn test_negative_integers() {
	assert_output("
-12 * -2 displayed
", "24\n");
}

#[test]
fn test_number_context_transmission() {
	assert_output("
sum is 1
2 + sum displayed
", "3\n");
}

#[test]
fn test_accessing_func_inside_func() {
	assert_output("
[x] doubled does:
	x * 2

[x] tripled does:
	x doubled + x

3 tripled displayed
", "9\n");
}

#[test]
fn test_accessing_var_inside_func() {
	assert_output("
doubler is 2
[x] doubled does:
	x * doubler

7 doubled displayed
", "nothing\n");
}

#[test]
fn test_basic_does() {
	assert_output("
data is:
	value is 11

[thing] operated does:
	thing is displayed
	value thing is * value thing
	value thing is displayed
	thing

data operated displayed
", "{'value'}\n121\n{'value'}\n")
}

#[test]
fn test_basic_does_takes_eval_ctx_as_input() {
	assert_output("
[x] squared does x * x

[x] summed does:
	#1 x + #2 x

result is:
	#1 is 7
	#2 is 8
	summed
	squared
	displayed
", "225\n");
}

#[test]
fn test_basic_if() {
	assert_output("
1 displayed if yes
2 displayed if no
sum is 3 if no
sum displayed
sum is 4 if yes
sum displayed
", "1\nnothing\n4\n");
}

#[test]
fn test_grouped_if() {
	assert_output("
1 displayed, if yes
2 displayed, if no
sum is 3, + 1, if yes
sum displayed
sum is 5, + 1, if no
sum displayed
", "1\n4\n5\n");
}

#[test]
fn test_inner_value_is() {
	assert_output("
A B C is 10
D is B C
A D is displayed
", "10\n")
}

#[test]
fn test_access_using_value_as_key_fails() {
	assert_output("
\"circle\" squared is \"pi = 3.2\"
y is \"circle\"
y squared displayed
", "{circle}\n")
}

#[test]
fn test_from() {
	assert_output("
\"circle\" described is \"pi = 3.2\"
selected shape is \"circle\"
selected shape from described displayed
", "pi = 3.2\n")
}

#[test]
fn test_second_order_eval_does() {
	assert_output("
\"circle\" squared does \"pi = 3.2\"
y is \"circle\"
y from squared displayed
", "pi = 3.2\n")
}

#[test]
fn test_input() {
	assert_io("
inputted displayed
", "Echo\n", "Echo\n")
}

#[test]
fn test_simple_gouping() {
	assert_output("
[n] squared is n * n
boop squared is 3
boop is 2

boop squared displayed
boop, squared displayed
boop; squared displayed
", "3\n4\n4\n")
}

#[test]
fn test_logic() {
	assert_output("
not yes displayed
not no displayed
yes and yes displayed
yes and no displayed
no and yes displayed
no and no displayed
yes or yes displayed
yes or no displayed
no or yes displayed
no or no displayed
", "no\nyes\nyes\nno\nno\nno\nyes\nyes\nyes\nno\n");
}

#[test]
fn test_scoping_getter() {
	assert_output("
yeehaw [tootin] does:
	rootin tootin

rootin is 12
yeehaw displayed
", "12\n");
}

#[test]
fn test_scoping_swap_operator() {
	assert_output("
Swapper is:
	[other] swapped with [me] does:
		temp is me
		me is other (should swap)
		temp
	root is 2

other is:
	root is 1
	swapped with Swapper

root other displayed
root Swapper displayed
", "2\n1\n");
}

#[test]
fn test_scoping_is_do_expr_after_do() {
	assert_output("
age is:
	do:
		year is 2022
		birth year is 1995
	year - birth year

age displayed
year age  displayed
", "27\n2022\n");
}

#[test]
fn test_scoping_is_do_ends_with_expr() {
	assert_output("
age is:
	do:
		year is 2022
		birth year is 1995
		year - birth year

age displayed
year age displayed
", "27\n2022\n");
}

#[test]
fn test_concat_nothing() {
	assert_output("
\"I regret \" & nothing displayed
", "nothing\n");
}

#[test]
fn test_scoping_is_is_does_1() {
	assert_output("
test is \"Test\"

Waffe is:
	inner is:
		greeting does:
			test & \" your might\"

greeting of inner Waffe displayed
", "Test your might\n")
}

#[test]
fn test_scoping_is_is_does_2() {
	assert_output("

Waffe is:
	test is \"Test\"
	inner is:
		greeting does:
			test & \" your might\"

greeting of inner Waffe displayed
", "nothing your might\n")
}

#[test]
fn test_scoping_is_is_does_3() {
	assert_output("

Waffe is:
	inner is:
		test is \"Test\"
		greeting does:
			test & \" your might\"

greeting of inner Waffe displayed
", "Test your might\n")
}

#[test]
fn test_is_field_then_value() {
	assert_output("
Deffie Default is:
	name is \"Deffie\"
	\"Default\"

Deffie Default displayed
name of Deffie Default displayed
", "Default\nDeffie\n");
}

#[test]
fn test_constructor_field_then_value() {
	assert_output("
[_] created is:
	name is \"Obby\"
	\"Object\"

Obby Object is created

Obby Object displayed
name of Obby Object displayed
", "Object\nObby\n");
}

#[test]
fn test_constructor_value_then_field() {
	assert_output("
[_] created is:
	\"Object\"
	name is \"Obby\"

Obby Object is created

Obby Object displayed
name of Obby Object displayed
", "Object\nObby\n");
}

#[test]
fn test_field_no_ref_to_container() {
	assert_output("
Waffe is:
	first name is \"Walter\"
	age is 26

age first name Waffe displayed
", "Walter\n");
}

#[test]
fn test_field_ref_to_container() {
	assert_output("
Waffe is:
	age is 26

first name Waffe is:
	age displayed
", "26\n"); // TODO: is this what we want?
}

#[test]
fn test_comma_if_value_persists() {
	assert_output("
x is 7
1, x * x, if x < 0
+ 1 displayed
", "2\n");
}

#[test]
fn test_field_into_primitive() {
	assert_output("
asd 1 is 10
asd 1 displayed
2 + 1 is displayed
", "10\n3\n")
}

#[test]
fn test_do_gives_value() {
	// 'do' should work exactly like a plain 'does'
	assert_output("
do 1
displayed
", "1\n")
}

#[test]
fn test_do_should_call() {
	// 'do' should work exactly like a plain 'does'
	assert_output("
number of visitors is 0

display greeting does \"Hello!\" displayed
display farewell does \"Today, we had \" & number of visitors & \" visitors!\" displayed

count visitor does:
	number of visitors is incremented
	nothing

do display greeting
do count visitor
do count visitor
do:
	number of visitors is incremented
do display farewell
", "Hello!\nToday, we had 3 visitors!\n")
}

#[test]
fn test_does_func_leaks_not_scope() {
	assert_output("
[_] asd does:
	secret is 123
	234

value is asd
secret value displayed ('secret' does not exist, so we display 'value')
", "234\n")
}

#[test]
fn test_does_getter_leaks_not_scope() {
	assert_output("
dsa is:
	asd [_] does:
		secret is 123
		234

value is asd dsa
secret value displayed ('secret' does not exist, so we display 'value')
", "234\n")
}

#[test]
fn test_do_with_return() {
	assert_output("
do:
	return 23
	12

displayed
", "23\n")
}

#[test]
fn test_do_scoping() {
	// 'do' should work exactly like a plain 'does'
	assert_output("
name is \"hehe\"
name Waffe is \"böhö\"
age Waffe is 23

do Waffe:
	name displayed
	age displayed
", "hehe\nnothing\n")
}

#[test]
fn test_basic_string_concat() {
	assert_output("
country [me] is \"Finland\"
\"Suomi = \" & country displayed
", "Suomi = Finland\n")
}

#[test]
fn test_return_at_end_of_does() {
	assert_output("
[x] very nice does:
	value is \"very nice\" displayed
	return 123

result is very nice
result displayed
value result displayed
", "very nice\n123\nnothing\n")
}

#[test]
fn test_basic_five_two() {
	assert_output("
value is 5 2
value displayed
", "2\n")
}

#[test]
fn test_basic_value_comma_if_no() {
	assert_output("
value is 5, 2 if no
value displayed
", "5\n")
}

#[test]
fn test_basic_value_comma_if_yes() {
	assert_output("
value is 5, 2 if yes
value displayed
", "2\n")
}

#[test]
fn test_empty_return() {
	assert_output("
[x] a1 does:
	\"entered1\" displayed
	return
	10 displayed

a1 displayed

a2 does:
	\"entered2\" displayed
	return
	10 displayed

a2 displayed
", "entered1\nnothing\nentered2\nnothing\n")
}


#[test]
fn test_return_if_no() {
	assert_output("
[input] checked does:
	return \"who?\" if no
	\"are\" displayed
	\"you\"

\"who\" checked displayed
", "are\nyou\n")
}

#[test]
fn test_return_if() {
	assert_output("
[nth] triangular number does:
	return 0 if nth < 1
	nth + 1 * nth / 2

-99 triangular number displayed
1 triangular number displayed
3 triangular number displayed
", "0\n1\n6\n")
}


#[test]
fn test_does_return() {
	assert_output("
proc does return 5, 4 if no

proc displayed
", "5\n")
}


#[test]
fn test_do_return() {
	assert_output("
proc does:
	do return 4 if yes

	5

proc displayed
", "4\n")
}

#[test]
fn test_return_from_procedure() {
	assert_output("
greet does:
	secret is 42
	return with:
		exit is 23
	\"dummy\"

greet displayed
", "{'exit'}\n")
}

#[test]
fn test_return_scoping_from_procedure() {
	assert_output("
value is 123
greet does:
	return with:
		exit is value + 1
	\"dummy\"

greeting is greet
exit greeting displayed
", "124\n")
}

#[test]
fn test_procedure_works_as_a_getter() {
	assert_output("
greet does:
	exit value is 6
	value

exit greet displayed
", "6\n")
}

#[test]
fn test_return_with_nested_if_no() {
	assert_output("
greet does:
	return if no:
		exit is 23
	\"dummy\"

greet displayed
exit displayed
", "dummy\nnothing\n")
}

#[test]
fn test_return_with_nested_if_yes() {
	assert_output("
greet does:
	return if yes:
		exit is 23
	\"dummy\"

greet displayed
exit displayed
", "{'exit'}\n23\n")
}

#[test]
fn test_funcs_from() {
	assert_output("
list is:
	#1 does \"Waffe\"
	#2 [me] does \"lingon\"
	#3 [me] does #1 me & \"?\"

index is #1
index from list displayed
index is incremented
index from list displayed
index is incremented
index from list displayed
", "Waffe\nlingon\nWaffe?\n")
}

#[test]
fn test_comma_in_left_side() {
	assert_output("
2, + 3 is displayed
", "5\n") // TODO: how should the assignment be handled?
}

#[test]
fn test_insert_new_field_from_method() {
	assert_output("
database is:
	\"Item database\"

	[_item] inserted to [me] does:
		_item displayed
		item me is _item
		me

10 inserted to database
item of database displayed
", "10\n10\n")
}

#[test]
fn test_do_if_equals() {
	assert_output("
confirmation is \"Y\"

do if confirmation = \"Y\":
	\"Nice\" displayed
", "Nice\n")
}