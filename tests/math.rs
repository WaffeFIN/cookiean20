mod common;
use common::{assert_output};

/**
 * This file contains extensive math related tests
 * 
 * The tests focus mostly on the standard library.
 */

#[test]
fn test_fraction_arithmetic() {
	assert_output("
0.05 + 0.95 is displayed
0.03 * 200:3 is displayed
13:5 - 1:2 + 0.9 is displayed
1 / 0.25 is displayed
1 / 1:5 is displayed
1.5 / 0.25 is displayed
", "1\n2\n3\n4\n5\n6\n");
}

#[test]
fn test_fraction_modulo() {
	assert_output("
123.45 modulo 23.45 displayed
123:45 modulo 23.45 displayed
123.45 modulo 23:45 displayed
123:45 modulo 23:45 displayed
0 - 123.45 modulo 23.45 displayed
0 - 123:50 modulo 23:50 displayed
", "6.2\n2.733333\n0.272222\n0.177777\n-6.2\n-0.16\n");
}

#[test]
fn test_fraction_div_by_zero() {
	assert_output("
1:0 displayed
1.0 / 0 displayed
1:1 / 0 displayed
0 - 1:0 displayed
0 - 1.0 / 0 displayed
0 - 1:1 / 0 displayed
", "inf\ninf\ninf\n-inf\n-inf\n-inf\n");
}

#[test]
fn test_roots() {
	assert_output("
#1 root of 0 displayed
#1 root of 123 displayed
#2 root of 16 displayed
#2 root of 1000 displayed
#3 root of 125 displayed
#5 root of 123456 displayed
", "0\n123\n4\n31.622776\n5\n10.430435\n");
}