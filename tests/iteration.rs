mod common;
use common::assert_output;

/**
 * This file contains all iteration and recursion related tests.
 * 
 * The tests focus mostly on the standard library.
 */

#[ignore]
#[test]
fn test_ackermann() {
	assert_output("
[pair] ackermann'd [again] does:
	[p] second'd [me] does:
		m is m p - 1
		n is 1
		ackermann'd again me
	[p] third'd [me] does:
		n is:
			m is m p
			n is n p - 1
			ackermann'd again me
		m is m p - 1
		ackermann'd again me
	m0 is m pair = 0
	n0 is n pair = 0
	mn is n pair * m pair > 0
	n pair + 1 if m0
	pair second'd if n0
	pair third'd if mn

result is:
	m is 3
	n is 3
	ackermann'd
	displayed
", "61\n");
}

#[test]
fn test_recursion_with_case() {
	assert_output("
0 factorial is 1
[n] factorial is n - 1 factorial * n

5 factorial displayed
", "120\n");
}

#[test]
fn test_recursion_with_if() {
	assert_output("
[x] triangled does:
	y is x - 1 triangled if x > 2
	x + y

5 triangled displayed
", "15\n");
}

#[test]
fn test_each_in() {
	assert_output("
num list has:
	0
	1
	3
	8

each in num list + 1 displayed
each in num list displayed
", "\n1\n2\n4\n9\n0\n1\n3\n8");
}

#[test]
fn test_each_in_assignment() {
	assert_output("
num list has:
	1
	2
	3

[x] squared does x * x

each in num list is - 7
each in num list displayed
each in num list is squared
each in num list displayed
", "-6\n-5\n-4\n36\n25\n16\n");
}