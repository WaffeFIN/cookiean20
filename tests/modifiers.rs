mod common;
use common::assert_output;

/**
 * This file contains tests of modifiers ("adverbs") like 'twice', 'if', 'continuously'
 * 
 * The tests focus mostly on the standard library.
 */

#[test]
fn test_twice() {
	assert_output("
[f] done twice:
	f, f

\"Hi\" displayed twice
", "Hi\nHi\n");
}

#[test]
fn test_do_twice() {
	assert_output("
[f] done twice:
	f, f

do twice:
	\"Hi\" displayed
", "Hi\nHi\n");
}

#[test]
fn test_do_multiple_twice() {
	assert_output("
[f] done twice:
	f, f

do twice, \"x\" displayed twice, twice; twice:
	\"Hi\" displayed
", "Hi\nHi\nx\nx\nx\nx\nHi\nHi\nx\nx\nx\nx\n")
}

#[test]
fn test_twice_twice() {
	assert_output("
[f] done twice:
	f, f

[n] doubled does 2 * n

3 doubled twice twice displayed
", "12\n")
}
