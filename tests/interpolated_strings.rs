mod common;
use common::assert_output;

/**
 * This file contains all string method and interpolated string related tests
 * 
 * The tests focus mostly on the standard library.
 */

#[test]
fn test_interpolated_string_eager_eval() {
	let code = "
Waffe is:
	name is:
		first is \"Walter\"
		last is \"Grönholm\"
		\"[first] [last]\"

name of Waffe displayed (Walter Grönholm)
first name Waffe is \"Waffe\"
name of Waffe displayed (Walter Grönholm)
";
	assert_output(code, "Walter Grönholm\nWalter Grönholm\n")
}

#[test]
fn test_basic_interpolated_string() {
	assert_output("\"[2 * 2]\" displayed
", "4\n");
}

#[test]
fn test_basic_string_concat_interpolated() {
	assert_output("
country [me] is \"Finland\"
\"Greetings\" & \" from [country]!\" displayed
", "Greetings from Finland!\n")
}

#[test]
fn test_interpolated_string_nothing() {
	assert_output("
\"I regret [nothing]\" displayed
", "I regret nothing\n");
}

#[test]
fn test_interpolated_string_if_yes() {
assert_output("
n is 123
\"[n]\" if yes displayed
", "123\n")
}

#[test]
fn test_interpolated_string_if_no() {
assert_output("
n is 123
\"[n]\" if no displayed
", "nothing\n")
}

#[test]
fn test_interpolated_string_as_key() {
	assert_output("
\"[\"heya\"]\" dict is \"I said hey!\"
dict is displayed
", "{heya}\n")
}