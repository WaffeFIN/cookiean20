extern crate baker;
	
use baker::tokenizing::lexer::Lexer;
use baker::tokenizing::structs::stringify_errors;
use baker::parsing::row_parser::parse as row_parse;
use baker::parsing::block_parser::parse as block_parse;
use baker::interpreting::crumbs_interpreter::run;

/**
 * This file contains helper functions for the other test files.
 * 
 * Tests that could fit into multiple files are placed in the lexicographically last file.
 */

 #[allow(dead_code)] // False positive
pub fn assert_output(code: &str, expected_output: &str) {
	assert_io(code, "", expected_output);
}

/**
 * Note: we are assuming the given code is interpretable. Any possible errors are suppressed.
 */
pub fn assert_io(code: &str, input: &str, expected_output: &str) {
	if !code.is_empty() {
		if let Some('\n') = code.chars().last() {
			/* All cool */
		} else {
			// This has happened one too many times...
			panic!("Remember to end the code with a new line");
		}
	}
	let tokens = Lexer::tokenize_cookiean(code).0;
	let rows   = row_parse(tokens).0;
	let root   = block_parse(rows).0;
	let crumbs = Vec::from(root);
	let mut std_in = std::io::Cursor::new(input);
	let mut std_out = Vec::new();
	run(crumbs, &mut std_in, &mut std_out, false);
	assert_eq!(std::str::from_utf8(&std_out).unwrap(), expected_output)
}

#[allow(dead_code)] // False positive
pub fn assert_compile_error(code: &str, expected_min_errors: usize, expected_output: Vec<&str>) {
	let (tokens, lexer_errors)       = Lexer::tokenize_cookiean(code);
	let (rows, row_parser_errors)    = row_parse(tokens);
	let (_root, block_parser_errors) = block_parse(rows);

	let (error_message, n_errors) = stringify_errors(vec![lexer_errors, row_parser_errors, block_parser_errors]);
	let mut output = error_message.clone();
	let mut assertion_errors = vec![];
	let mut ok = true;

	for expected_part in expected_output {
		if let Some(idx) = output.find(expected_part) {
			assertion_errors.push(format!("Found: {}", expected_part));
			output = output.split_off(idx + expected_part.len());
		} else {
			assertion_errors.push(format!("Couldn't find: {}", expected_part));
			ok = false;
		}
	}
	assert!(
		ok,
		"{:#?}\nOutput:\n{}",
		assertion_errors, error_message
	);
	//the exact amount of errors is not that important
	let expected_max_errors = expected_min_errors + expected_min_errors / 2;
	
	assert!(
		n_errors >= expected_min_errors && n_errors <= expected_max_errors,
		"Expected {} to {} errors but got {}",
		expected_min_errors, expected_max_errors, n_errors
	);
}