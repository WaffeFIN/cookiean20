
use std::rc::Rc;
use crate::generating::structs::{ Crumb, Key };
use crate::interpreting::segmented_stack::SegmentedStack;
use crate::interpreting::standard_cookie::get_standard_library;
use crate::interpreting::structs::*;

pub fn run<'a>(
	instructions: Vec<Crumb<'a>>,
	std_in: &'a mut dyn std::io::BufRead,
	std_out: &'a mut dyn std::io::Write,
	debug_instruction: bool
) {
	let mut data_ctxs = DataContexts::default();

	Interpreter {
		row: 1,
		column: 1,
		standard_library: get_standard_library(&mut data_ctxs),
		data_ctxs: data_ctxs,
		environment: Environment {
			instructions: instructions,
			std_in: std_in,
			std_out: std_out
		},
		object_stack: SegmentedStack::with(Object::new_empty()),
	}.run(debug_instruction);
}

const KEYS_NOT_EMPTY: &'static str = "Keys should not be empty";
const SEGMENT_NOT_EMPTY: &'static str = "Current segment should not be empty";
const SEGMENT_SHOULD_HAVE_TWO: &'static str = "Current segment should have at least two objects";
const PREVIOUS_SEGMENT_NOT_EMPTY: &'static str = "Previous segment should not be empty";
const AT_LEAST_TWO_SEGMENTS: &'static str = "There should be more than one segment";

struct Interpreter<'a> {
	row: usize,
	column: usize,
	environment: Environment<'a>,
	data_ctxs: DataContexts<'a>,
	standard_library: Object<'a>,
	/// Call stack and object stack in one
	object_stack: SegmentedStack<Object<'a>>
}
impl<'a> Interpreter<'a> {
	fn debug_instruction(&self, crumb: &Crumb<'a>) {
		println!(
			"{:?}\n@{}:{} Crumb #{}:  \t{:?}",
			self.object_stack,
			self.row,
			self.column,
			self.environment.instructions.len(),
			crumb,
		);
	}

	fn run(&mut self, debug_instruction: bool) -> Object<'a> {	
		while let Some(instruction) = self.environment.instructions.pop() {
			if debug_instruction {
				self.debug_instruction(&instruction);
			}
			match instruction {
				Crumb::Enter{ keys } => {
					self.enter(keys);
				}
				Crumb::EnterBeneath { keys } => {
					let current_object = self.object_stack.pop().expect(SEGMENT_NOT_EMPTY);
					self.enter(keys);
					self.object_stack.push(current_object);
				}
				Crumb::PopEnter => {
					let current = self.object_stack.pop().expect(SEGMENT_NOT_EMPTY);
					if let Data::Key(key) = &current.reference.borrow().data {
						self.enter(vec![key.clone()].into());
					} else {
						panic!("Expected current object to be a key")
					}; // This semicolon is peculiar... needed for borrow checker
				}
				Crumb::PreEval { row, column } => {
					// Marks the current row and column. Pushes segment
					self.row = row;
					self.column = column;
					self.object_stack.push_segment(/* Popped in Eval */);
				}
				Crumb::Find { reverse_keys } => {
					let eval_ctx = self.get_eval_context();
					// First: get from current execution context (current_object, parents etc.)
					let item = reverse_keys.last().expect(KEYS_NOT_EMPTY); 
					let current_value = self.get(&eval_ctx, item);

					self.find(eval_ctx, current_value, &reverse_keys[..reverse_keys.len()-1]);
				}
				Crumb::PopFind { reverse_keys } => {
					let eval_ctx = self.get_eval_context();

					// First: get current object, which is the result of a getter
					let current_value = self.object_stack.pop().expect(SEGMENT_NOT_EMPTY);

					self.find(eval_ctx, current_value, &reverse_keys);
				}
				Crumb::Separator { column, .. } => {
					// If encountered here, just use it as a column marker
					self.column = column;
				}
				Crumb::Eval { row, column } => {
					// Evaluates the last segment in the object stack into a single result
					// The result is pushed onto the object stack
					self.row = row;
					self.column = column;
					let result = self.squash_eval();
					self.object_stack.truncate_segment(/* Pushed in PreFind */);
					self.object_stack.push(result);
				}
				Crumb::Meld { exits } => {
					self.meld();
					self.exit(exits);
				}
				Crumb::Binding { back, name } => {
					// Sets an unbound binding for the current object
					let current = self.object_stack.last_mut().expect(SEGMENT_NOT_EMPTY);
					let binding = if let Data::Modifier(_, _) = current.reference.borrow().data {
						Binding::UnboundModifier { left: name }
					} else {
						Binding::Unbound { back: back, name: name }
					};
					current.set_binding(binding);
				}
				Crumb::InlineGetter(crumbs) => {
					// Pushes the getter on the object stack using eval_ctx as declaration ctx
					let declaration_ctx = self.get_eval_context();
					let method = Getter::Cookiean { crumbs, declaration_ctx };
					self.object_stack.push(Object::new_with(Data::Getter(
						method,
						Binding::Unbound { back: 0, name: vec![].into() }
					)));
				}
				Crumb::Getter(crumbs) => {
					// Pushes the getter on the object stack
					let declaration_ctx = self.get_declaration_context();
					let method = Getter::Cookiean { crumbs, declaration_ctx };
					self.object_stack.push(Object::new_with(Data::Getter(method, Binding::Missing)));
				}
				Crumb::InlineFunction(crumbs) => {
					// Pushes the function on the object stack using eval_ctx as declaration ctx
					let declaration_ctx = self.get_eval_context();
					let function = Function::Cookiean { crumbs, declaration_ctx };
					self.object_stack.push(Object::new_with(Data::Function(function, None)));
				}
				Crumb::Function(crumbs) => {
					// Pushes the function on the object stack
					let declaration_ctx = self.get_declaration_context();
					let function = Function::Cookiean { crumbs, declaration_ctx };
					self.object_stack.push(Object::new_with(Data::Function(function, None)));
				}
				Crumb::ModifierFn(crumbs) => {
					// Pushes the modifier on the object stack
					let declaration_ctx = self.get_declaration_context();
					let modifier = Modifier::CookieanFn { crumbs, declaration_ctx };
					self.object_stack.push(Object::new_with(Data::Modifier(modifier, Binding::Missing)));
				}
				Crumb::ModifierOp(crumbs) => {
					// Pushes the modifier on the object stack
					let declaration_ctx = self.get_declaration_context();
					let modifier = Modifier::CookieanOp { crumbs, declaration_ctx };
					self.object_stack.push(Object::new_with(Data::Modifier(modifier, Binding::Missing)));
				}
				Crumb::Discard { exits } => {
					// Pops #exits objects
					self.exit(exits);
				}
				Crumb::EnterContext => {
					// Pops the current object and appends it to the parent object's contexts
					// Assumptions:
					// -- 2+ objects in segment
					let eval_result = self.object_stack.pop().expect(SEGMENT_NOT_EMPTY);
					self.object_stack.last_mut().expect(SEGMENT_SHOULD_HAVE_TWO).add_context(eval_result);
				}
				Crumb::Return => {
					if self.object_stack.last().is_none() {
						panic!("self.object_stack.last().is_none()");
					}
					let segment = self.object_stack.current_segment().expect(SEGMENT_NOT_EMPTY);
					if segment.len() != 2 {
						panic!("Crumb::Return segment.len() != 2");
					}
					if Rc::ptr_eq(&segment[0].reference, &segment[1].reference) {
						/* Do not return with the scope of the function */
						continue;
					}
					let mut drain_idx: Option<usize> = None;
					for (idx, crumb) in self.environment.instructions.iter().enumerate().rev() {
						if let Crumb::OutOfCall { .. } = crumb {
							drain_idx = Some(idx);
							break;
						}
					}
					if let Some(drain_idx) = drain_idx {
						self.environment.instructions.drain(drain_idx + 1..);
					} else {
						panic!("There are no OutOfCalls to Return out from")
					}
				}
				Crumb::OutOfCall { row, column } => {
					// Marks the end of a procedure call
					// Assumptions:
					// -- 2+ segments
					self.row = row;
					self.column = column;

					let eval_result = self.object_stack.pop().expect(SEGMENT_NOT_EMPTY);
					self.object_stack.truncate_segment(/* Pushed in .call() */);
					self.object_stack.push(eval_result);
				}
				Crumb::OutOfModifier { row, column } => {
					// Drains current segment and pops it
					// Truncates next segment
					// Pops the input value (for the modified function)
					// Appends the drained segment
					// Pushes the input value
					// Assumptions:
					// -- 2+ segments
					self.row = row;
					self.column = column;

					let drained_segment = self.object_stack.drain_current_segment();
					self.object_stack.pop_segment();
					self.object_stack.truncate_segment();
					let input_object = self.object_stack.pop().expect(SEGMENT_NOT_EMPTY);
					self.object_stack.extend(drained_segment);
					self.object_stack.push(input_object);
				}
				Crumb::Bind { modifier, into } => {
					if let Some(Binding::UnboundModifier { left }) = modifier.get_binding() {
						let left_object = self.object_stack.swap(into, modifier);
						let modifier_scope = Object::new_scope(&left, left_object);
						let binding = Binding::Bound { scope: modifier_scope };
						
						let modifier = self.object_stack.get_mut(into);
						let data = modifier.reference.borrow().data.clone();
						let mut new_modifier = Object::new_with(data);
						new_modifier.set_binding(binding);
						self.object_stack.swap(into, new_modifier);
					} else {
						panic!("Unexpected binding in Bind crumb at {}:{}", self.row, self.column);
					}
				}
			}
		}
		if debug_instruction {
			println!("{:?}", self.object_stack);
		}
		self.object_stack.pop().expect(SEGMENT_NOT_EMPTY)
	}

	fn enter(&mut self, keys: Rc<[Key<'a>]>) {
		for key in keys.iter() {
			let current = self.object_stack.last_mut().expect(SEGMENT_NOT_EMPTY);
			let target = current.get_or_create(key);
			self.object_stack.push(target);
		}
	}

	fn meld(&mut self) {
		let right = self.object_stack.pop().expect(SEGMENT_NOT_EMPTY);
		let left = self.object_stack.last_mut().expect(SEGMENT_SHOULD_HAVE_TWO);
		left.meld_with(right);
	}

	fn exit(&mut self, mut exits: usize) {
		while exits > 0 {
			self.object_stack.pop();
			exits -= 1;
		}
	}

	fn get_eval_context(&self) -> Object<'a> {
		self.object_stack.previous_segment().expect(AT_LEAST_TWO_SEGMENTS)
		                             .last().expect(PREVIOUS_SEGMENT_NOT_EMPTY).clone()
	}

	fn get_declaration_context(&self) -> Object<'a> {
		let mut ctx = self.object_stack.first_in_segment()
			.cloned()
			.unwrap_or(Object::new_empty()); // Empty object is needed as in 'do twice: ...' case
		ctx.editable = false;
		ctx
	}

	/// Should only be called from a Find
	fn get(&self, source: &Object<'a>, key: &Key<'a>) -> Object<'a> {
		source.get(
			&self.data_ctxs, key
		).or_else(
			|| {
				// Note: that this is only called from Find, where
				//       current_segment() will contain the current eval, not previous frames
				let parent_objects = self.object_stack.previous_segment()?;
				for parent in parent_objects.iter().rev() {
					if let Some(result) = parent.get(&self.data_ctxs, key) {
						return Some(result)
					}
				}
				None
			}
		).or_else(
			|| { self.standard_library.get(&self.data_ctxs, key) }
		).unwrap_or(
			match key {
				Key::Word(_) => { Object::new_empty() }
				_ => { Object::new_with(Data::Key(key.clone())) }
			}
		)
	}

	fn find(&mut self, eval_ctx: Object<'a>, mut current_value: Object<'a>, keys: &[Key<'a>]) {
		let mut remaining_items = keys.len();
		loop {
			// Next: get nested objects
			let mut values: Vec<Object<'a>> = vec![eval_ctx.clone(), current_value];

			while remaining_items > 0 {
				let key = &keys[remaining_items-1];
				
				if let Some(new_value) = values.last().unwrap().get(&self.data_ctxs, key) {
					remaining_items -= 1;
					values.push(new_value);
					continue;
				}
				break;
			}
			let bound_result = self.bind(remaining_items, values);
			if let Some(bound_result) = bound_result {
				let result = self.get_right(bound_result, &keys[..remaining_items]);
				if let Some(result) = result {
					self.object_stack.push(result); // TODO: make the return more meaningful
				} else {
					// None has special meaning: interrupt Find to run other instructions
					return
				}
			}
			if remaining_items == 0 {
				break
			}
			// First: get from current execution context (current_object, parents etc.)
			let item = &keys[remaining_items-1]; remaining_items -= 1; 
			current_value = self.get(&eval_ctx, item);
		}
	}

	/// values is a non-empty vec
	/// 
	/// Returns the last item in values, bound to its back reference
	fn bind(&mut self, remaining_items: usize, mut values: Vec<Object<'a>>) -> Option<Object<'a>> {
		let binding = values.last().unwrap().get_binding();
		match binding {
			None => { /* Do nothing */ }
			Some(Binding::Missing) => {
				panic!("Missing binding at {}:{}", self.row, self.column);
			}
			Some(Binding::Bound { scope }) => {
				panic!("Function already bound w/ {:?} at {}:{}", scope, self.row, self.column);
			}
			Some(Binding::Unbound { back, name }) => {
				let back_reference = &values[values.len() - back - 1];
				let scope = Object::new_scope(&name, back_reference.clone());
				let data = values.pop().unwrap().reference.borrow().data.clone();
				let mut bound_object = Object::new_with(data);
				bound_object.set_binding(Binding::Bound{ scope: scope });
				return Some(bound_object);
			}
			Some(Binding::UnboundModifier { left }) => {
				if remaining_items == 0 {
					let next_crumb = self.environment.instructions.pop().unwrap();
					let mut crumbs = vec![Crumb::Meld { exits: 0 }];
					
					if let Crumb::Separator { n_crumbs, column } = next_crumb {
						let idx = self.environment.instructions.len() - n_crumbs;
						crumbs.push(Crumb::Eval { row: self.row, column: column });
						crumbs.extend(self.environment.instructions.drain(idx..));
					} else {
						crumbs.push(Crumb::Eval { row: self.row, column: self.column });
						crumbs.push(next_crumb);
					}
					crumbs.push(Crumb::PreEval { row: self.row, column: self.column });
					crumbs.push(Crumb::Meld { exits: 0 });
					// TODO: Above might cause some problems.
					//       Needed to provide context to modifier parameter
					
					// Not a Getter as this is only eval'd, not found. If that's a problem, calc it first.
					let left_object = Object::new_with(Data::Function(
						Function::Cookiean {
							crumbs: crumbs.into(),
							declaration_ctx: self.get_eval_context()
						},
						None
					));
					let modifier_scope = Object::new_scope(&left, left_object);
					let binding = Binding::Bound { scope: modifier_scope };
					let data = values.pop().unwrap().reference.borrow().data.clone();
					let mut modifier = Object::new_with(data);
					modifier.set_binding(binding);
					return Some(modifier);
				}
				self.environment.instructions.push(Crumb::Bind {
					modifier: values.pop().unwrap(),
					into: self.object_stack.len()
				});
				return None;
			}
		}
		Some(values.pop().unwrap())
	}

	/// Handle first stage of ForEach, Getters and Modifiers
	/// 
	/// Returns None if BuildEval should be stopped
	fn get_right(&mut self, current_value: Object<'a>, remaining_items: &[Key<'a>]) -> Option<Object<'a>> {
		match &current_value.reference.borrow().data {
			Data::ForEach { .. } => {
				todo!()
			}
			Data::Getter(Getter::Native(native), Binding::Bound { scope }) => {
				let result = native(&mut self.environment, scope);
				return Some(result)
			}
			Data::Getter(Getter::Cookiean { crumbs, declaration_ctx }, Binding::Bound { scope }) => {
				let mut scope = scope.clone();
				scope.add_context(declaration_ctx.clone()); // TODO: needs to be removed (before exit or what?)
				self.queue_getter_call(crumbs, scope, remaining_items);
				return None
			}
			_ => { /* No special treatment for you! */ }
		}
		return Some(current_value)
	}

	fn queue_getter_call(&mut self, crumbs: &Rc<[Crumb<'a>]>, scope: Object<'a>, remaining_items: &[Key<'a>]) {
		if remaining_items.len() > 0 {
			self.environment.instructions.push(Crumb::PopFind{
				reverse_keys: remaining_items.into()
			});
		}
		self.environment.instructions.push(Crumb::OutOfCall{ row: self.row, column: self.column });
		self.environment.instructions.extend(crumbs.iter().cloned());
		self.object_stack.push_segment(/* Popped in OutOfCall */);
		self.object_stack.push(scope);
	}

	fn squash_eval(&mut self) -> Object<'a> {
		let mut eval_result = self.object_stack.pop().expect(SEGMENT_NOT_EMPTY);
		
		// We might need to start the squash with the current object (aka eval ctx)
		// Squash eval begins with current object if the top of eval stack expects an input
		if eval_result.reference.borrow().data.will_take_input() {
			eval_result = self.left_eval(self.get_eval_context(), eval_result);
		}
		
		while let Some(obj) = self.object_stack.pop() {
			eval_result = self.left_eval(eval_result, obj);
		}
		eval_result
	}

	/// Handle Errors, Functions and Modifiers
	fn left_eval(&mut self, left: Object<'a>, right: Object<'a>) -> Object<'a> {
		let mut propagate_value = false;
		if let Data::Error(_) = &left.reference.borrow().data {
			propagate_value = true;
		}
		if propagate_value {
			return left;
		}
		
		let right_ref = right.reference.borrow();
		match &right_ref.data {
			//
			//   ====== Functions ======
			//   ||                   ||
			Data::Function(Function::Cookiean { crumbs, declaration_ctx }, None) => {
				let mut scope = Object::new_empty();
				scope.add_context(declaration_ctx.clone());
				self.queue_function_call(crumbs, scope);
				left
			}
			Data::Function(Function::Cookiean { crumbs, declaration_ctx }, Some(Binding::Bound { scope })) => {
				let mut scope = scope.clone();
				scope.add_context(declaration_ctx.clone());
				self.queue_function_call(crumbs, scope);
				left
			}
			Data::Function(Function::FromBinding, Some(Binding::Bound { scope })) => {
				if let Data::Key(key) = &left.reference.borrow().data {
					self.queue_from_call(key.clone());
					scope.clone()
				} else {
					panic!("Attempted to use 'from' with non-key value at {}:{}", self.row, self.column);
				}
			}
			Data::Function(Function::NativeFn(native), None) => {
				{
					if let Data::ForEach { filter: _, source } = &left.reference.borrow().data {
						let new_properties = source.reference.borrow().properties
							.iter()
							.filter(|(_item, _value)| true) // TODO filter
							.map(|(item, value)| (item.clone(), native(&mut self.environment, &value) ))
							.collect::<Children>();
						return Object::new_with(Data::ForEach {
							filter: Object::new_empty(),
							source: Object::with_properties(new_properties, source.editable)
						})
					}
				}
				native(&mut self.environment, &left)
			}
			Data::Function(Function::NativeOp(native), Some(Binding::Bound { scope })) => {
				{
					if let Data::ForEach { filter: _, source } = &left.reference.borrow().data {
						let new_properties = source.reference.borrow().properties
							.iter()
							.filter(|(_item, _value)| true) // TODO filter
							.map(|(item, value)| (item.clone(), native(&mut self.environment, &value, &scope) ))
							.collect::<Children>();
						return Object::new_with(Data::ForEach {
							filter: Object::new_empty(),
							source: Object::with_properties(new_properties, source.editable)
						})
					}
				}
				native(&mut self.environment, &left, &scope)
			}
			Data::Function(_, _) => {
				panic!("Attempted calculation with unbound function at {}:{}", self.row, self.column);
			}
			//
			//   ====== Modifiers ======
			//   ||                   ||
			Data::Modifier(Modifier::NativeFn(modifier), Binding::Bound { scope }) => {
				let modified_function = modifier(&mut self.environment, &scope);
				self.object_stack.push(modified_function);
				left
			}
			Data::Modifier(Modifier::NativeOp(modifier), Binding::Bound { scope }) => {
				let right_expression = self.squash_eval(); // <3 recursion

				let modified_function = modifier(&mut self.environment, &scope, &right_expression);
				self.object_stack.push(modified_function);
				left
			}
			Data::Modifier(Modifier::CookieanFn { crumbs, declaration_ctx }, Binding::Bound { scope }) => {
				self.object_stack.push(left); // save and swap later
				let mut scope = scope.clone();
				scope.add_context(declaration_ctx.clone());
				self.queue_modifier_call(crumbs, scope);

				self.environment.instructions.push(Crumb::Discard { exits: 1 }); // Discard dummy object below
				Object::new_with(Data::Missing) // Dummy object
			}
			Data::Modifier(Modifier::CookieanOp { crumbs, declaration_ctx }, Binding::Bound { scope }) => {
				let right_expression = self.squash_eval(); // <3 recursion

				self.object_stack.push(left); // save and swap later
				let mut scope = scope.clone();
				scope.add_context(declaration_ctx.clone());
				self.queue_modifier_call(crumbs, scope);

				right_expression
			}
			Data::Modifier(_, _) => {
				panic!("Attempted calculation with unbound modifier at {}:{}", self.row, self.column);
			}
			//
			//   ====== Other ======
			//   ||               ||
			_ => {
				drop(right_ref);
				right
			}
		}
	}

	/// May only be called when in an Eval
	fn queue_function_call(&mut self, crumbs: &Rc<[Crumb<'a>]>, bound_object: Object<'a>) {
		self.object_stack.push_segment(/* Popped in OutOfCall */);
		self.object_stack.push(bound_object);
		self.object_stack.push_segment(/* Counteracts the truncate_segment at the end of current Eval */);
		self.environment.instructions.push(Crumb::Eval { row: self.row, column: self.column });
		self.environment.instructions.push(Crumb::OutOfCall { row: self.row, column: self.column });
		self.environment.instructions.extend(crumbs.iter().cloned());
	}

	/// May only be called when in an Eval
	fn queue_modifier_call(&mut self, crumbs: &Rc<[Crumb<'a>]>, bound_object: Object<'a>) {
		self.object_stack.push_segment(/* Popped in OutOfCall */);
		self.object_stack.push(bound_object);
		self.object_stack.push_segment(/* Counteracts the truncate_segment at the end of current Eval */);
		self.environment.instructions.push(Crumb::Eval { row: self.row, column: self.column });
		self.environment.instructions.push(Crumb::OutOfModifier { row: self.row, column: self.column } );
		self.environment.instructions.extend(crumbs.iter().cloned());
	}

	// May only be called when in an Eval
	fn queue_from_call(&mut self, key: Key<'a>) {
		self.object_stack.push_segment(/* Popped in OutOfCall */);
		self.object_stack.push_segment(/* Counteracts the truncate_segment at the end of current Eval */);
		self.environment.instructions.push(Crumb::Eval { row: self.row, column: self.column });
		self.environment.instructions.push(Crumb::OutOfCall { row: self.row, column: self.column });
		self.environment.instructions.push(Crumb::PopFind { reverse_keys: vec![key].into() });
		// as the bound object is returned by the current Eval, it will be used in PopFind
	}
}
