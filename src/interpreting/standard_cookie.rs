
extern crate num_rational;
use std::borrow::Cow;
use num_rational::Rational64;

extern crate num_traits;
use num_traits::cast::ToPrimitive;

use crate::generating::structs::Key;
use crate::interpreting::structs::*;

macro_rules! fn_num {
	($f_name:ident: $var:ident => $nbody:block $fbody:block $abody:block) => {
		fn $f_name<'a>(_environment: &mut Environment, val: &Object<'a>) -> Object<'a> {
			let data = &val.reference.borrow().data;
			Object::new_with(match data {
				Data::Key(Key::Ordinal($var)) => {
					Data::Key(Key::Ordinal($nbody))
				}
				Data::Key(Key::Fraction($var)) => {
					Data::Key(Key::Fraction($fbody))
				}
				Data::Approx($var) => {
					Data::Approx($abody)
				}
				_ => Data::Error(format!("'{}' expects a number but got {:?}", stringify!($f_name), data))
			})
		}
	}
}

macro_rules! key {
	($word:expr) => {
		Key::Word($word)
	}
}

macro_rules! insert_property {
	($properties:ident, $f_name:ident: getter) => (
		$properties.insert(
			key!(stringify!($f_name)),
			Object::new_with(Data::Getter(
				Getter::Native($f_name),
				Binding::Unbound { back: 1, name: Vec::new().into() } // TODO multi words back
			))
		)
	);
	($properties:ident, $f_name:ident: fn) => (
		$properties.insert(
			key!(stringify!($f_name)),
			Object::new_with(Data::Function(
				Function::NativeFn($f_name),
				None
			))
		)
	);
	($properties:ident, $f_name:ident: op) => (
		$properties.insert(
			key!(stringify!($f_name)),
			Object::new_with(Data::Function(
				Function::NativeOp($f_name),
				Some(Binding::Unbound { back: 1, name: Vec::new().into() })
			))
		)
	);
	($properties:ident, $f_word:tt = $f_name:ident: modifier) => (
		$properties.insert(
			key!($f_word),
			Object::new_with(Data::Modifier(
				Modifier::NativeOp($f_name),
				// TODO what here?
				Binding::UnboundModifier { left: Vec::new().into() }
			))
		)
	);
	($properties:ident, $f_word:tt = $f_name:ident: op) => (
		$properties.insert(
			key!($f_word),
			Object::new_with(Data::Function(
				Function::NativeOp($f_name),
				Some(Binding::Unbound { back: 1, name: Vec::new().into() })
			))
		)
	);
	($properties:ident, $f_word:tt = $f_name:ident: fn) => (
		$properties.insert(
			key!($f_word),
			Object::new_with(Data::Function(
				Function::NativeFn($f_name),
				None
			))
		)
	);
}

macro_rules! num_op {
	($op_name:ident: $x:ident $sym:tt $y:ident) => {
		fn $op_name<'a>(_environment: &mut Environment, left: &Object<'a>, right: &Object<'a>) -> Object<'a> {
			let left = &left.reference.borrow().data;
			let right = &right.reference.borrow().data;
			Object::new_with(match (left, right) {
				(Data::Key(Key::Ordinal($x)), Data::Key(Key::Ordinal($y))) => {
					Data::Key(Key::Ordinal($x $sym $y))
				}
				(Data::Key(Key::Fraction($x)), Data::Key(Key::Fraction($y))) => {
					Data::Key(Key::Fraction($x $sym $y))
				}
				(Data::Key(Key::Fraction($x)), Data::Approx($y)) => {
					Data::Approx($x.to_f64().unwrap() $sym $y)
				}
				(Data::Approx($x), Data::Key(Key::Fraction($y))) => {
					Data::Approx($x $sym $y.to_f64().unwrap())
				}
				(Data::Approx($x), Data::Approx($y)) => {
					Data::Approx($x $sym $y)
				}
				_ => Data::Error(format!("You can't '{}' a {:?} with {:?}", stringify!($op_name), left, right))
			})
		}
	};
}

macro_rules! frac_op {
	($op_name:ident: $x:ident, $y:ident => $f_body:block) => {
		fn $op_name<'a>(_environment: &mut Environment, left: &Object<'a>, right: &Object<'a>) -> Object<'a> {
			let left = &left.reference.borrow().data;
			let right = &right.reference.borrow().data;
			Object::new_with(match (left, right) {
				(Data::Key(Key::Fraction($x)), Data::Key(Key::Fraction($y))) => {
					Data::Key(Key::Fraction($f_body))
				}
				_ => Data::Error(format!("You can't '{}' a {:?} with {:?}", stringify!($op_name), left, right))
			})
		}
	};
	($op_name:ident: $x:ident, $y:ident ~> $f_body:block) => {
		fn $op_name<'a>(_environment: &mut Environment, left: &Object<'a>, right: &Object<'a>) -> Object<'a> {
			let left = &left.reference.borrow().data;
			let right = &right.reference.borrow().data;
			Object::new_with(match (left, right) {
				(Data::Key(Key::Fraction($x)), Data::Key(Key::Fraction($y))) => {
					Data::Approx($f_body)
				}
				_ => Data::Error(format!("You can't '{}' a {:?} with {:?}", stringify!($op_name), left, right))
			})
		}
	}
}

macro_rules! cmp_op {
	($op_name:ident: $x:ident $sym:tt $y:ident) => {
		fn $op_name<'a>(_environment: &mut Environment, left: &Object<'a>, right: &Object<'a>) -> Object<'a> {
			let left = &left.reference.borrow().data;
			let right = &right.reference.borrow().data;
			Object::new_with(match (left, right) {
				(Data::Key(Key::Ordinal($x)), Data::Key(Key::Ordinal($y))) => {
					Data::Key(Key::Boolean($x $sym $y))
				}
				(Data::Key(Key::Fraction($x)), Data::Key(Key::Fraction($y))) => {
					Data::Key(Key::Boolean($x $sym $y))
				}
				(Data::Key(Key::Fraction($x)), Data::Approx($y)) => {
					Data::Key(Key::Boolean($x.to_f64().unwrap() $sym *$y))
				}
				(Data::Approx($x), Data::Key(Key::Fraction($y))) => {
					Data::Key(Key::Boolean(*$x $sym $y.to_f64().unwrap()))
				}
				(Data::Approx($x), Data::Approx($y)) => {
					Data::Key(Key::Boolean($x $sym $y))
				}
				(Data::Key(Key::Text($x)), Data::Key(Key::Text($y))) => {
					Data::Key(Key::Boolean($x.len() $sym $y.len()))
				}
				(Data::Key(Key::Fraction($x)), Data::Key(Key::Text($y))) => {
					Data::Key(Key::Boolean(*$x $sym Rational64::from_integer($y.len() as i64)))
				}
				(Data::Key(Key::Text($x)), Data::Key(Key::Fraction($y))) => {
					Data::Key(Key::Boolean(Rational64::from_integer($x.len() as i64) $sym *$y))
				}
				(Data::Bytes($x), Data::Bytes($y)) => {
					Data::Key(Key::Boolean($x.len() $sym $y.len()))
				}
				(Data::Key(Key::Fraction($x)), Data::Bytes($y)) => {
					Data::Key(Key::Boolean(*$x $sym Rational64::from_integer($y.len() as i64)))
				}
				(Data::Bytes($x), Data::Key(Key::Fraction($y))) => {
					Data::Key(Key::Boolean(Rational64::from_integer($x.len() as i64) $sym *$y))
				}
				_ => Data::Error(format!("You can't '{}' a {:?} with {:?}", stringify!($op_name), left, right))
			})
		}
	};
}

macro_rules! bool_getter {
	($getter_name:ident: $var:ident => $f_body:block) => {
		fn $getter_name<'a>(_environment: &mut Environment, val: &Object<'a>) -> Object<'a> {
			let val = &val.reference.borrow().data;
			Object::new_with(match val {
				Data::Key(Key::Boolean($var)) => {
					Data::Key(Key::Boolean($f_body))
				}
				_ => panic!("Underlying data should be Boolean when accessing getter '{}'. Got: {:?}", stringify!($getter_name), val)
			})
		}
	};
}

macro_rules! bool_op {
	($op_name:ident: $x:ident $sym:tt $y:ident) => {
		fn $op_name<'a>(_environment: &mut Environment, left: &Object<'a>, right: &Object<'a>) -> Object<'a> {
			let left = &left.reference.borrow().data;
			let right = &right.reference.borrow().data;
			Object::new_with(match (left, right) {
				(Data::Key(Key::Boolean($x)), Data::Key(Key::Boolean($y))) => {
					Data::Key(Key::Boolean(*$x $sym *$y))
				}
				_ => Data::Error(format!("You can't '{}' a {:?} with {:?}", stringify!($op_name), left, right))
			})
		}
	};
}

macro_rules! veclike_to_frac_getter {
	($getter_name:ident: $var:ident => $f_body:block) => {
		fn $getter_name<'a>(_environment: &mut Environment, val: &Object<'a>) -> Object<'a> {
			let val = &val.reference.borrow().data;
			Object::new_with(match val {
				Data::Key(Key::Text($var)) => {
					Data::Key(Key::Fraction( Rational64::from_integer($f_body) ))
				}
				Data::Bytes($var) => {
					Data::Key(Key::Fraction( Rational64::from_integer($f_body) ))
				}
				_ => panic!("Underlying data should be Text or Bytes when accessing getter '{}'. Got: {:?}", stringify!($getter_name), val)
			})
		}
	};
}

macro_rules! error_to_text_getter {
	($getter_name:ident: $var:ident => $f_body:block) => {
		fn $getter_name<'a>(_environment: &mut Environment, val: &Object<'a>) -> Object<'a> {
			let val = &val.reference.borrow().data;
			Object::new_with(match val {
				Data::Error($var) => {
					Data::Key(Key::Text(( $f_body ).into()))
				}
				_ => panic!("Underlying data should be Error when accessing getter '{}'. Got: {:?}", stringify!($getter_name), val)
			})
		}
	};
}

#[allow(unused_mut)]
pub fn get_standard_library<'a>(data_ctxs: &mut DataContexts<'a>) -> Object<'a> {
	{	// Approx
		let mut properties = &mut data_ctxs.get_context(
			&Data::Approx(f64::default())
		).unwrap().reference.borrow_mut().properties;
		insert_property!(properties, "+" = plus: op);
		insert_property!(properties, "-" = minus: op);
		insert_property!(properties, "/" = divide: op);
		insert_property!(properties, "*" = multiply: op);
		insert_property!(properties, "=" = equals: op);
		insert_property!(properties, "<" = smaller_than: op);
		insert_property!(properties, ">" = greater_than: op);
		insert_property!(properties, "&" = amp: op);
	}
	{	// Boolean
		let mut properties = &mut data_ctxs.get_context(
			&Data::Key(Key::Boolean(bool::default()))
		).unwrap().reference.borrow_mut().properties;
		insert_property!(properties, and: op);
		insert_property!(properties, or: op);
		insert_property!(properties, not: getter);
		insert_property!(properties, "&" = amp: op);
	}
	{	// Fraction
		let mut properties = &mut data_ctxs.get_context(
			&Data::Key(Key::Fraction(Rational64::from_integer(0)))
		).unwrap().reference.borrow_mut().properties;
		insert_property!(properties, modulo: op);
		insert_property!(properties, root: op);

		insert_property!(properties, "+" = plus: op);
		insert_property!(properties, "-" = minus: op);
		insert_property!(properties, "/" = divide: op);
		insert_property!(properties, "*" = multiply: op);
		insert_property!(properties, "=" = equals: op);
		insert_property!(properties, "<" = smaller_than: op);
		insert_property!(properties, ">" = greater_than: op);
		insert_property!(properties, "&" = amp: op);
	}
	{	// Ordinal
		let mut properties = &mut data_ctxs.get_context(
			&Data::Key(Key::Ordinal(usize::default()))
		).unwrap().reference.borrow_mut().properties;
		insert_property!(properties, "+" = plus: op);
		insert_property!(properties, "-" = minus: op);
		insert_property!(properties, "/" = divide: op);
		insert_property!(properties, "*" = multiply: op);
		insert_property!(properties, "=" = equals: op);
		insert_property!(properties, "<" = smaller_than: op);
		insert_property!(properties, ">" = greater_than: op);
		insert_property!(properties, "&" = amp: op);
	}
	{	// Text
		let mut properties = &mut data_ctxs.get_context(
			&Data::Key(Key::Text("".into()))
		).unwrap().reference.borrow_mut().properties;
		insert_property!(properties, length: getter);
		insert_property!(properties, "<" = smaller_than: op);
		insert_property!(properties, ">" = greater_than: op);
		insert_property!(properties, "=" = equals: op);
		insert_property!(properties, "&" = amp: op);
	}
	{	// Bytes
		let mut properties = &mut data_ctxs.get_context(
			&Data::Bytes(Vec::default())
		).unwrap().reference.borrow_mut().properties;
		insert_property!(properties, length: getter);
		insert_property!(properties, "<" = smaller_than: op);
		insert_property!(properties, ">" = greater_than: op);
	}
	{	// Error
		let mut properties = &mut data_ctxs.get_context(
			&Data::Error(String::default())
		).unwrap().reference.borrow_mut().properties;
		insert_property!(properties, "&" = amp: op);
		insert_property!(properties, message: getter);
	}
	{	// Nothing (Object)
		let mut properties = &mut data_ctxs.get_context(
			&Data::Nothing
		).unwrap().reference.borrow_mut().properties;
		insert_property!(properties, from: getter);
	}
	// TODO insert types (a number) to std
	let mut result = Object::new_with(Data::Missing);
	{
		let mut properties = &mut result.reference.borrow_mut().properties;
		insert_property!(properties, displayed: fn);
		insert_property!(properties, inputted: fn);
		insert_property!(properties, debugged: fn);
		insert_property!(properties, incremented: fn);
		insert_property!(properties, "if" = cookiean_if: modifier);
		insert_property!(properties, missing: getter);
	}
	result
}

fn displayed<'a>(environment: &mut Environment, val: &Object<'a>) -> Object<'a> {
	writeln!(&mut environment.std_out, "{}", val).expect(FAILED_TO_WRITE);
	val.clone()
}

fn inputted<'a>(environment: &mut Environment, val: &Object<'a>) -> Object<'a> {
	if let Some(prompt) = val.reference.borrow().properties.get(&Key::Word("prompt".into())) {
		displayed(environment, &prompt);
	}
	let mut line: String = String::new();
	environment.std_in.read_line(&mut line).expect("Failed to read input");
	let newline_chars: &[char] = &['\n', '\r'];
	let text: Cow<'a, str> = line.trim_end_matches(newline_chars).to_owned().into();
	Object::new_with(Data::Key(Key::Text(text)))
}

fn debugged<'a>(environment: &mut Environment, val: &Object<'a>) -> Object<'a> {
	debug_print(&mut environment.std_out, None, val, 0);
	val.clone()
}

fn_num!(incremented: n => { n + 1 } { n + Rational64::from_integer(1) } { n + 1.0f64 });

#[allow(dead_code)]
fn a<'a>(_environment: &mut Environment, _val: &Object<'a>, _this: &Object<'a>) -> Object<'a> {
	// recursively set "missing" value for each in object
	todo!()
}

#[allow(dead_code)]
fn each<'a>(_environment: &mut Environment, this: &Object<'a>) -> Object<'a> {
	// TODO: what here?
	// each person is displayed
	// how to give current object to this..?
	// name of each person is displayed
	this.clone()
}

#[allow(dead_code)]
fn cookiean_in<'a>(_environment: &mut Environment, val: &Object<'a>, this: &Object<'a>) -> Object<'a> {
	Object::new_with(Data::ForEach {
		filter: val.clone(),
		source: this.clone()
	})
}

#[allow(dead_code)]
fn cookiean_each_in<'a>(_environment: &mut Environment, this: &Object<'a>) -> Object<'a> {
	let allow_all: Object = Object::new_with(Data::Function(Function::NativeFn(
		|_, _| Object::new_with(Data::Key(Key::Boolean(true)))
	), None));
	cookiean_in(_environment, &allow_all, this)
}

fn amp<'a>(_environment: &mut Environment, val: &Object<'a>, this: &Object<'a>) -> Object<'a> {
	Object::new_with(Data::Key(Key::Text(format!("{}{}", val, this).into())))
}

fn from<'a>(_environment: &mut Environment, this: &Object<'a>) -> Object<'a> {
	Object::new_with(Data::Function(
		Function::FromBinding,
		Some(Binding::Bound { scope: this.clone() })
	))
}

#[allow(dead_code)]
fn eq<'a>(_environment: &mut Environment, _val: &Object<'a>, _this: &Object<'a>) -> Object<'a> { todo!() }

#[allow(dead_code)]
fn cookiean_as<'a>(_environment: &mut Environment, _val: &Object<'a>, _this: &Object<'a>) -> Object<'a> { todo!() }

fn identity<'a>(_environment: &mut Environment, val: &Object<'a>) -> Object<'a> {
	val.clone()
}

fn cookiean_if<'a>(_environment: &mut Environment, procedure: &Object<'a>, expr: &Object<'a>) -> Object<'a> {
	if let Data::Key(Key::Boolean(true)) = expr.reference.borrow().data {
		procedure.clone()
	} else {
		Object::new_with(Data::Function(Function::NativeFn(identity), None))
	}
}

fn missing<'a>(_environment: &mut Environment, _this: &Object<'a>) -> Object<'a> {
	Object::new_with(Data::Missing)
}

num_op!(plus: x + y);
num_op!(minus: x - y);
num_op!(divide: x / y);
num_op!(multiply: x * y);
frac_op!(modulo: x, y => { x % y });
frac_op!(root: x, y ~> { y.to_f64().unwrap().powf(x.recip().to_f64().unwrap()) });
cmp_op!(smaller_than: x < y);
cmp_op!(greater_than: x > y);
cmp_op!(equals: x == y);
bool_getter!(not: x => { !x });
bool_op!(and: x && y);
bool_op!(or: x || y);
veclike_to_frac_getter!(length: s => { s.len() as i64 });
error_to_text_getter!(message: e => { e.to_string() });

pub fn debug_print<'a>(out: & mut dyn std::io::Write, field: Option<String>, value: &Object<'a>, mut depth: usize) {
	if depth > 12 {
		return
	}
	let value = value.reference.borrow();
	if let Some(field) = field {
		writeln!(out, "{} {}:", "==".repeat(depth), field).expect(FAILED_TO_WRITE);
		depth += 1;
	}
	writeln!(out, "{} content:  {}", "-|".repeat(depth), value.data).expect(FAILED_TO_WRITE);
	if !value.properties.is_empty() {
		writeln!(out, "{} properties:", "-|".repeat(depth)).expect(FAILED_TO_WRITE);
		for (key, value) in value.properties.iter() {
			debug_print(out, Some(format!("{}", key)), value, depth + 1);
		}
	}
}

const FAILED_TO_WRITE: &'static str = "Failed to write to output";
