
extern crate ahash;
use self::ahash::AHasher;

use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt;
use std::hash::BuildHasherDefault;
use std::rc::{Rc, Weak};

use crate::generating::structs::{Crumb, Key};
// TODO use these
// type Set<Item> = HashSet<Item, BuildHasherDefault<AHasher>>;
// type PtrSet<'a> = Set<*mut ObjectData<'a>>;
type Map<K, V> = HashMap<K, V, BuildHasherDefault<AHasher>>;
pub type Children<'a> = Map<Key<'a>, Object<'a>>;
type SelfReference<'a> = Weak<RefCell<ObjectData<'a>>>;

#[derive(Debug)]
pub struct Object<'a> {
	pub reference: Rc<RefCell<ObjectData<'a>>>,
	pub editable: bool,
}
impl<'a> Clone for Object<'a> {
	fn clone(&self) -> Self {
		Object {
			reference: Rc::clone(&self.reference),
			editable: self.editable
		}
	}
}
impl<'a> TryFrom<&SelfReference<'a>> for Object<'a> {
	type Error = ();

	fn try_from(self_ref: &SelfReference<'a>) -> Result<Object<'a>, ()> {
		Ok(Object { reference: self_ref.upgrade().ok_or(())?, editable: true })
	}
}

impl<'a> Object<'a> {
	fn new(data: ObjectData, editable: bool) -> Object {
		Object {
			reference: Rc::new(RefCell::new(data)),
			editable: editable
		}
	}

	pub fn with_properties(properties: Children, editable: bool) -> Object {
		Object::new(ObjectData {
			properties: properties,
			contexts: Vec::new(),
			data: Data::Nothing
		}, editable)
	}

	pub fn new_scope(self_reference: &Rc<[Key<'a>]>, value: Object<'a>) -> Object<'a> {
		if self_reference.is_empty() {
			return value;
		}

		let result: Object<'_> = Object::new_empty();
		let mut current = result.clone();
		for key in self_reference.iter().skip(1).rev() {
			let property = Object::new_empty();
			current.reference.borrow_mut().properties.insert(key.clone(), property.clone());
			current = property;
		}
		current.reference.borrow_mut().properties.insert(self_reference[0].clone(), value);
		current
	}

	pub fn new_with(content: Data<'a>) -> Object<'a> {
		Object::new(ObjectData {
			properties: Map::default(),
			contexts: Vec::new(),
			data: content
		}, true)
	}

	pub fn new_empty() -> Object<'a> {
		Object::new_with(Data::Nothing)
	}

	pub fn get_binding(&self) -> Option<Binding<'a>> {
		return match &self.reference.borrow().data {
			Data::Function(_, Some(binding)) |
			Data::Getter(_, binding)         |
			Data::Modifier(_, binding) => {
				Some(binding.clone())
			}
			_ => { None }
		};
	}

	pub fn set_binding(&mut self, binding: Binding<'a>) {
		let bound_data = match &self.reference.borrow().data {
			Data::Function(function, _) => {
				Data::Function(function.clone(), Some(binding))
			}
			Data::Getter(getter, _) => {
				Data::Getter(getter.clone(), binding)
			}
			Data::Modifier(modifier, _) => {
				Data::Modifier(modifier.clone(), binding)
			}
			_ => { return } // TODO Error?
		};
		self.reference.borrow_mut().data = bound_data;
	}

	pub fn get(&self, data_ctxs: &DataContexts<'a>, left: &Key) -> Option<Object<'a>> {
		let right = self.reference.borrow();
		// Get from properties first
		if let Some(result) = right.properties.get(left) {
			return Some(result.clone())
		}
		// Second, get from the context of the data
		if let Some(result) = data_ctxs.get_context(&right.data).and_then(|ctx| ctx.get(data_ctxs, left)) {
			return Some(result)
		}
		// Lastly, go though the custom contexts. TODO: wrong order? Shouldn't custom ctxs be prioritized over default
		let mut contexts = right.contexts.iter();
		while let Some(context) = contexts.next() {
			let result = context.get(data_ctxs, left);
			
			// TODO: During iteration, already iterated contexts should not be iterated again
			if result.is_some() {
				return result
			}
		}
		None
	}

	pub fn meld_with(&mut self, other: Object<'a>) {
		if Rc::ptr_eq(&self.reference, &other.reference) {
			return // Do not merge with itself
		}
		let borrow = other.reference.borrow();
		self.reference.borrow_mut().data = borrow.data.clone();
	
		self.reference.borrow_mut().properties.extend(
			borrow.properties.iter().map(|(k, v)| (k.clone(), v.clone()))
		); // TODO smarter
	}

	pub fn get_or_create(&mut self, key: &Key<'a>) -> Object<'a> {
		match self.reference.borrow().properties.get(key) {
			Some(property) => return property.clone(),
			None => { /* Go out of borrow */ }
		}
		let property = if let Key::Word(_) = key {
			Object::new_empty()
		} else {
			/* Text, Boolean, Fraction, Ordinal */
			Object::new_with(Data::Key(key.clone()))
		};
		self.reference.borrow_mut().properties.insert(key.clone(), property.clone());
		property
	}

	pub fn add_context(&mut self, mut ctx: Object<'a>) {
		if Rc::ptr_eq(&self.reference, &ctx.reference) {
			return;
		}

		ctx.editable = false;
		self.reference.borrow_mut().contexts.push(ctx);
	}
}
impl fmt::Display for Object<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.reference.borrow())
	}
}

pub struct ObjectData<'a> {
	pub data: Data<'a>,
	pub properties: Children<'a>,
	contexts: Vec<Object<'a>>,
}
impl fmt::Debug for ObjectData<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{{{:?}, {}}}", self.data, self.properties.keys().map(|key| key.to_string()).collect::<Vec<String>>().join(", "))
	}
}
impl fmt::Display for ObjectData<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self.data {
			Data::Nothing => {
				if self.properties.is_empty() {
					write!(f, "nothing")
				} else {
					write!(f, "{{{}}}", self.properties.keys().map(|key| key.to_string()).collect::<Vec<String>>().join(", "))
				}
			}
			_ => {
				write!(f, "{}", self.data)
			}
		}
	}
}

#[derive(Clone)]
pub enum Getter<'a> {
	Native   (fn(&mut Environment, &Object<'a>) -> Object<'a>),
	Cookiean { crumbs: Rc<[Crumb<'a>]>, declaration_ctx: Object<'a> },
}
impl fmt::Debug for Getter<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match &self {
			Getter::Native   (native)       => write!(f, "Getter::Native {:#p}", native),
			Getter::Cookiean { crumbs, .. } => write!(f, "Getter::Cookiean [{}]", crumbs.len()),
		}
	}
}
impl fmt::Display for Getter<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match &self {
			Getter::Native(native)          => write!(f, "{:#p}", native),
			Getter::Cookiean { crumbs, .. } => write!(f, "@{}", crumbs.len()),
		}
	}
}

#[derive(Clone, Debug)]
pub enum Binding<'a> {
	/// Temporary value
	Missing,
	Unbound { back: usize, name: Rc<[Key<'a>]> },
	UnboundModifier { left: Rc<[Key<'a>]> },
	/// In NativeOp, scope is used as the right operand instead.
	Bound { scope: Object<'a> },
}

#[derive(Clone)]
/// Takes an object from the left. Might be bound
pub enum Function<'a> {
	NativeFn(fn(&mut Environment, &Object<'a>) -> Object<'a>),
	NativeOp(fn(&mut Environment, &Object<'a>, &Object<'a>) -> Object<'a>),
	Cookiean { crumbs: Rc<[Crumb<'a>]>, declaration_ctx: Object<'a> },
	FromBinding, // special function, as the environment does not contain the required data
}
impl fmt::Debug for Function<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match &self {
			Function::NativeFn(native) => write!(f, "Function::NativeFn {:#p}", native),
			Function::NativeOp(native) => write!(f, "Function::NativeOp {:#p}", native),
			Function::Cookiean { crumbs, .. }
			                           => write!(f, "Function::Cookiean [{}]", crumbs.len()),
			Function::FromBinding      => write!(f, "Function::FromBinding"),
		}
	}
}
impl fmt::Display for Function<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match &self {
			Function::NativeFn(native) => write!(f, "{:#p}", native),
			Function::NativeOp(native) => write!(f, "{:#p}", native),
			Function::Cookiean { crumbs, .. }
			                           => write!(f, "@{}", crumbs.len()),
			Function::FromBinding      => write!(f, "from"),
		}
	}
}

#[derive(Clone)]
pub enum Modifier<'a> {
	NativeFn(fn(&mut Environment, &Object<'a>) -> Object<'a>),
	NativeOp(fn(&mut Environment, &Object<'a>, &Object<'a>) -> Object<'a>),
	CookieanFn { crumbs: Rc<[Crumb<'a>]>, declaration_ctx: Object<'a> },
	CookieanOp { crumbs: Rc<[Crumb<'a>]>, declaration_ctx: Object<'a> },
}
impl fmt::Debug for Modifier<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match &self {
			Modifier::NativeFn(native) => write!(f, "Modifier::NativeFn {:#p}", native),
			Modifier::NativeOp(native) => write!(f, "Modifier::NativeOp {:#p}", native),
			Modifier::CookieanFn { crumbs, .. } => write!(f, "Modifier::CookieanFn [{}]", crumbs.len()),
			Modifier::CookieanOp { crumbs, .. } => write!(f, "Modifier::CookieanOp [{}]", crumbs.len()),
		}
	}
}
impl fmt::Display for Modifier<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match &self {
			Modifier::NativeFn(native) => write!(f, "{:#p}", native),
			Modifier::NativeOp(native) => write!(f, "{:#p}", native),
			Modifier::CookieanFn { crumbs, .. } => write!(f, "@{}", crumbs.len()),
			Modifier::CookieanOp { crumbs, .. } => write!(f, "@{}", crumbs.len()),
		}
	}
}

#[derive(Clone, Debug)]
pub enum Data<'a> {
	// Denotes a value that should be there but isn't.
	// It is also used by contexts to stop 'get' from propagating
	Missing,
	// Denotes a value that deliberately is not there
	Nothing,
	Key(Key<'a>),
	Error(String), // TODO: Add parent error stack?
	Approx(f64),
	Bytes(Vec<u8>),
	// Only references self. Called on Find allowing further access of fields from result
	// e.g.
	// 1 + length of  "Hello, World!" displayed (--> 14)
	//
	// Interpolated strings are also methods:
	// length of  "Hello, ["World"]!" displayed (--> 13)
	Getter(Getter<'a>, Binding<'a>),
	// Called on Eval. Takes input from left, might be bound
	Function(Function<'a>, Option<Binding<'a>>),
	// Called on Eval. Takes input from left, might be bound
	Modifier(Modifier<'a>, Binding<'a>),
	// Iterator of each 'filter'-like property in 'source'.
	// Allows further access of fields from it as it would be a 'filter'-like Object<'a>
	// e.g.
	// a person has a name
	// name of  each person in personell list is displayed
	ForEach { filter: Object<'a>, source: Object<'a> },
}

impl std::fmt::Display for Data<'_> {

	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		use self::Data as D;
		match self {
			D::Missing                       => write!(f, "missing"),
			D::Nothing                       => write!(f, "nothing"),
			D::Key(val)                      => write!(f, "{}", val),
			D::Error(message)                => write!(f, "Error: {}", message),
			D::Approx(val)                   => write!(f, "{:.6}", val),
			D::Bytes(data)                   => write!(f, "[{}B]", data.len()),
			D::Getter(getter, Binding::Bound { .. })            => write!(f, "{}[..]", getter),
			D::Getter(getter, _)                                => write!(f, "{}[??]", getter),
			D::Function(function, Some(Binding::Bound { .. }))  => write!(f, "[..]{}[..]", function),
			D::Function(function, Some(_))                      => write!(f, "[..]{}[??]", function),
			D::Function(function, None)                         => write!(f, "[..]{}", function),
			D::Modifier(modifier, Binding::Bound { .. })        => write!(f, "[@@]{}[..]", modifier),
			D::Modifier(modifier, _)                            => write!(f, "[@@]{}[??]", modifier),
			D::ForEach { filter, .. }        => write!(f, "each<{}>in[..]", filter),
		}
	}
}
impl Data<'_> {
	pub fn will_take_input(&self) -> bool {
		use self::Data as D;
		match self {
			D::Function(_, _) |
			D::Modifier(_, _) => true, // Modifiers return functions which then will take input
			_ => false
		}
	}
}

/// Contains the primitives' contexts
pub struct DataContexts<'a> {
	ordinal_ctx: Object<'a>,
	fraction_ctx: Object<'a>,
	boolean_ctx: Object<'a>,
	text_ctx: Object<'a>,
	error_ctx: Object<'a>,
	approx_ctx: Object<'a>,
	bytes_ctx: Object<'a>,
	object_ctx: Object<'a>,
	// TODO types ctx and for each ctx
}
impl<'a> Default for DataContexts<'a> {
	fn default() -> Self {
		DataContexts {
			ordinal_ctx:  Object::new_empty(),
			boolean_ctx:  Object::new_empty(),
			text_ctx:     Object::new_empty(),
			error_ctx:    Object::new_empty(),
			approx_ctx:   Object::new_empty(),
			bytes_ctx:    Object::new_empty(),
			fraction_ctx: Object::new_empty(),
			object_ctx:   Object::new_with(Data::Missing),
		}
	}
}
impl<'a> DataContexts<'a> {
	pub fn get_context(&self, content: &Data) -> Option<&Object<'a>> {
		use crate::generating::structs::Key::*;
		match content {
			Data::Missing                => None,
			Data::Key(Ordinal(_))        => Some(&self.ordinal_ctx),
			Data::Key(Fraction(_))       => Some(&self.fraction_ctx),
			Data::Key(Boolean(_))        => Some(&self.boolean_ctx),
			Data::Key(Text(_))           => Some(&self.text_ctx),
			Data::Error(_)               => Some(&self.error_ctx),
			Data::Approx(_)              => Some(&self.approx_ctx),
			Data::Bytes(_)               => Some(&self.bytes_ctx),
			Data::ForEach { filter, .. } => self.get_context(&filter.reference.borrow().data),
			_                            => Some(&self.object_ctx)
		}
	}
}

/// Contains handles which allow built-in functions to access, for instance, standard input/output
pub struct Environment<'a> {
	pub instructions: Vec<Crumb<'a>>,
	pub std_in: &'a mut dyn std::io::BufRead,
	pub std_out: &'a mut dyn std::io::Write,
}