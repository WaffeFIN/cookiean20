
use std::fmt;

/// A vec divided into segments, where elements can only be popped from the last segment
pub struct SegmentedStack<T> {
	data: Vec<T>,
	segments: Vec<usize>
}
impl<T> SegmentedStack<T> {

    #[inline]
	pub fn with(t: T) -> SegmentedStack<T> {
		SegmentedStack {
			data: vec![t],
			segments: vec![0]
		}
	}

    #[inline]
	pub fn push(&mut self, t: T) {
		self.data.push(t);
	}

    #[inline]
	pub fn extend(&mut self, t: Vec<T>) {
		self.data.extend(t);
	}

    #[inline]
	pub fn pop(&mut self) -> Option<T> {
		if self.data.len() > *self.segments.last().unwrap() {
			self.data.pop()
		} else {
			None
		}
	}

    #[inline]
	pub fn first_in_segment(&self) -> Option<&T> {
		self.data.get(*self.segments.last()?)
	}

    #[inline]
	pub fn last(&self) -> Option<&T> {
		self.data.last()
	}

    #[inline]
	pub fn last_mut(&mut self) -> Option<&mut T> {
		self.data.last_mut()
	}

    #[inline]
	pub fn push_segment(&mut self) {
		self.segments.push(self.data.len())
	}

	/// Pushes a duplicate of the previous segment
    #[inline]
	pub fn push_duplicate_segment(&mut self) {
		self.segments.push(*self.segments.last().unwrap())
	}

    #[inline]
	pub fn truncate_segment(&mut self) {
		let pop_until = self.segments.pop().unwrap();
		self.data.truncate(pop_until);
	}

    #[inline]
	pub fn truncate_snd_last_segment(&mut self) {
		let pop_end = self.segments.pop().unwrap();
		let pop_start = *self.segments.last().unwrap();
		self.data.drain(pop_start..pop_end);
	}

    #[inline]
	pub fn pop_segment(&mut self) -> Option<usize> {
		self.segments.pop()
	}

    #[inline]
	pub fn current_segment(&self) -> Option<&[T]> {
		Some(&self.data[*self.segments.last()?..])
	}

    #[inline]
	pub fn swap(&mut self, idx: usize, mut obj: T) -> T {
		std::mem::swap(self.get_mut(idx), &mut obj);
		obj
	}

    #[inline]
	pub fn get_mut(&mut self, idx: usize) -> &mut T {
		&mut self.data[idx]
	}

    #[inline]
	pub fn drain_current_segment(&mut self) -> Vec<T> {
		self.data.drain(*self.segments.last().unwrap()..).collect()
	}

    #[inline]
	pub fn previous_segment(&self) -> Option<&[T]> {
		let length = self.segments.len();
		if length < 2 {
			return None
		}
		Some(&self.data[*self.segments.get(length - 2)?..*self.segments.last()?])
	}

    #[inline]
	pub fn len(&self) -> usize {
		self.data.len()
	}

    #[inline]
	pub fn n_segs(&self) -> usize {
		self.segments.len()
	}
}
impl<T: fmt::Display> fmt::Debug for SegmentedStack<T> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let mut data_strs = vec![];
		let mut seg_strs = vec![];
		let mut seg_idx = 0;
		for (data_idx, elem) in self.data.iter().enumerate() {
			let annotate_segment = seg_idx < self.segments.len() && *&self.segments[seg_idx] == data_idx;
			while seg_idx < self.segments.len() && *&self.segments[seg_idx] == data_idx {
				seg_idx += 1
			}
			let result = format!(" {} ", elem);
			seg_strs.push(format!("{}{}", if annotate_segment { " ^" } else { "  " }, " ".repeat(result.len() - 2)));
			data_strs.push(result);
		}
		if let Some(idx) = self.segments.last() {
			if *idx == self.data.len() {
				seg_strs.push(String::from(" ^"));
			}
		}
		write!(f, "{}\n{}", data_strs.join("|"), seg_strs.join(" "))
	}
}