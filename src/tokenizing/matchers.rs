
use crate::tokenizing::structs::*;
use enum_dispatch::enum_dispatch;

#[enum_dispatch]
pub trait Matcher {
	fn reset(&mut self) { /* Nada */ }
	fn test(&mut self, input: char) -> Status<Type>;
}

#[enum_dispatch(Matcher)]
pub enum Matchers {
	WhitespaceMatcher,
	SimpleMatcher,
	WordMatcher,
	IntegralMatcher,
	OrdinalMatcher,
	StringMatcher,
	SequenceMatcher,
}

#[derive(PartialEq, Clone)]
pub enum Status<T> where T: Clone {
	MatchContinue(T),
	MatchStop(T),
	Continue,
	Stop,
}

/// Matcher for non-line breaking whitespace
pub struct WhitespaceMatcher {
	tabs: bool,
	others: bool,
}
impl WhitespaceMatcher {
	pub fn new() -> Self {
		WhitespaceMatcher {
			tabs: false,
			others: false,
		}
	}
}
impl Matcher for WhitespaceMatcher {	
	fn reset(&mut self) {
		self.tabs = false;
		self.others = false;
	}

	fn test(&mut self, character: char) -> Status<Type> {
		match character {
			'\t' => {
				self.tabs = true;
				Status::MatchContinue(if self.others {
					Type::TabsMixedWhitespace
				} else {
					Type::Tabs
				})
			}
			_ if character.is_whitespace() && character != '\n' => {
				self.others = true;
				Status::MatchContinue(if self.tabs {
					Type::TabsMixedWhitespace
				} else {
					Type::NonTabWhitespace
				})
			}
			_ => Status::Stop
		}
	}
}

/// Matches the given string
pub struct SimpleMatcher {
	chars: Vec<char>,
	index: usize,
	token_type: Type
}
impl SimpleMatcher {
	pub fn new(word: &str, token_type: Type) -> SimpleMatcher {
		SimpleMatcher {
			chars: word.chars().collect(),
			index: 0,
			token_type: token_type
		}
	}
}
impl Matcher for SimpleMatcher {	
	fn reset(&mut self) {
		self.index = 0;
	}

	fn test(&mut self, character: char) -> Status<Type> {
		if self.chars[self.index] == character {
			self.index += 1;
			if self.index == self.chars.len() {
				Status::MatchStop(self.token_type)
			} else {
				Status::Continue
			}
		} else {
			Status::Stop
		}
	}
}

fn is_wordlike(character: char) -> bool {
	match character {
		_ if character.is_whitespace() | character.is_control() => false,
		'(' | ')' | '[' | ']' | '{' | '}' => false,
		'.' | ',' | ':' | ';' | '?' | '!' | '|' => false,
		'"' => false,
		_ => true
	}
}

/// Matches any valid Cookiean word
pub struct WordMatcher;
impl Matcher for WordMatcher {
	fn test(&mut self, character: char) -> Status<Type> {
		if is_wordlike(character) {
			Status::MatchContinue(Type::Word)
		} else {
			Status::Stop
		}
	}
}

/// Matches any sequence of digits
pub struct IntegralMatcher {
	allow_sign: bool
}
impl IntegralMatcher {
	pub fn new() -> IntegralMatcher {
		IntegralMatcher {
			allow_sign: true
		}
	}
}
impl Matcher for IntegralMatcher {
	fn reset(&mut self) {
		self.allow_sign = true;
	}

	fn test(&mut self, character: char) -> Status<Type> {
		match character {
			_ if character.is_ascii_digit() => Status::MatchContinue(Type::Integral),
			'+' | '-' if self.allow_sign => {
				self.allow_sign = false;
				Status::Continue
			}
			_ => Status::Stop
		}
	}
}

/// Matches ordinals like #1 or #00
pub struct OrdinalMatcher {
	expect_integral: bool
}
impl OrdinalMatcher {
	pub fn new() -> OrdinalMatcher {
		OrdinalMatcher {
			expect_integral: false
		}
	}
}
impl Matcher for OrdinalMatcher {
	fn reset(&mut self) {
		self.expect_integral = false;
	}

	fn test(&mut self, character: char) -> Status<Type> {
		match character {
			'#' if !self.expect_integral => {
				self.expect_integral = true;
				Status::Continue
			},
			_ if self.expect_integral && character.is_ascii_digit() => Status::MatchContinue(Type::Ordinal),
			_ => Status::Stop
		}
	}
}

/// Examples: 
/// "hello"
/// ""
pub struct StringMatcher {
	started: bool,
	should_start_with: char,
	should_end_with: char,
	token_type: Type,
}
impl StringMatcher {
	pub fn new(starts_with: char, ends_with: char, token_type: Type) -> StringMatcher {
		StringMatcher {
			started: false,
			should_start_with: starts_with,
			should_end_with: ends_with,
			token_type,
		}
	}
}
impl Matcher for StringMatcher {
	fn reset(&mut self) {
		self.started = false;
	}
	
	fn test(&mut self, character: char) -> Status<Type> {
		if !self.started {
			if self.should_start_with == character {
				self.started = true;
				Status::Continue
			} else {
				Status::Stop
			}
		} else {
			if self.should_end_with == character {
				Status::MatchStop(self.token_type)
			} else if '\n' == character {
				Status::Stop
			} else if is_string_terminator(character) {
				Status::Stop
			} else {
				Status::Continue
			}
		}
	}
}

fn is_string_terminator(c: char) -> bool {
	c == '[' || c == ']' || c == '"'
}

/// Matches any sequence of word-likes separated by a given separator
/// E.g. '.' separated sequences matches 0.2 0.0.0.2 0..2 .2 ..2
pub struct SequenceMatcher{
	splitting_char: char,
	token_type: Type
}
impl SequenceMatcher {
	pub fn new(splitting_char: char, token_type: Type) -> SequenceMatcher {
		SequenceMatcher {
			splitting_char: splitting_char,
			token_type: token_type
		}
	}
}
impl Matcher for SequenceMatcher {
	fn test(&mut self, character: char) -> Status<Type> {
		if character == self.splitting_char {
			Status::Continue
		} else if is_wordlike(character) {
			Status::MatchContinue(self.token_type)
		} else {
			Status::Stop
		}
	}
}