
use std::iter::Peekable;
use std::str::CharIndices;
use crate::tokenizing::structs::*;
use crate::tokenizing::matchers::*;

macro_rules! token_error {
	($lexer:ident, $($arg:tt)*) => (
		Error { error_type: "Tokenizing".to_owned(), message: format!($($arg)*), row: $lexer.row, column: $lexer.column }
	)
}

/// A struct capable of lexing or tokenizing code 
pub struct Lexer<'a> {
	code: &'a str,
	chars: Peekable<CharIndices<'a>>,
	tokens: Vec<Token<'a>>,
	errors: Vec<Error>,
	row: usize,
	column: usize
}
impl<'a> Lexer<'a> {
	/// returns: A vector of tokens along with any tokenization errors
	pub fn tokenize_cookiean(code: &str) -> (Vec<Token>, Option<Errors>) {
		let mut lexer = Lexer {
			code: code,
			chars: code.char_indices().peekable(),
			tokens: vec![],
			errors: vec![],
			row: 1,
			column: 1
		};

		let matchers: Vec<Matchers> = vec![
			SequenceMatcher::new('.', Type::DotSequence).into(),
			SequenceMatcher::new(':', Type::ColonSequence).into(),
			WordMatcher.into(),
			StringMatcher::new('"', '"', Type::StringLiteral).into(),
			StringMatcher::new('"', '[', Type::InterpolatedStringLeft).into(),
			OrdinalMatcher::new().into(),
			IntegralMatcher::new().into(),
			SimpleMatcher::new("return", Type::Return).into(),
			SimpleMatcher::new("with", Type::With).into(),
			// TODO: means
			SimpleMatcher::new("done", Type::Done).into(),
			SimpleMatcher::new("uses", Type::Uses).into(),
			SimpleMatcher::new("does", Type::Does).into(),
			SimpleMatcher::new("some", Type::A).into(),
			SimpleMatcher::new("use", Type::Use).into(),
			SimpleMatcher::new("has", Type::Has).into(),
			SimpleMatcher::new("the", Type::The).into(),
			SimpleMatcher::new("yes", Type::Yes).into(),
			SimpleMatcher::new("an", Type::A).into(),
			SimpleMatcher::new("do", Type::Do).into(),
			SimpleMatcher::new("is", Type::Is).into(),
			SimpleMatcher::new("no", Type::No).into(),
			SimpleMatcher::new("of", Type::Of).into(),
			SimpleMatcher::new("by", Type::By).into(),
			SimpleMatcher::new("a", Type::A).into(),
			WhitespaceMatcher::new().into(),
			SimpleMatcher::new("\n", Type::NewLine).into(),
			SimpleMatcher::new("(", Type::ParenthesisOpen).into(),
			SimpleMatcher::new(")", Type::ParenthesisClose).into(),
			SimpleMatcher::new("[", Type::BracketOpen).into(),
			SimpleMatcher::new("]", Type::BracketClose).into(),
			SimpleMatcher::new("{", Type::BraceOpen).into(),
			SimpleMatcher::new("}", Type::BraceClose).into(),
			SimpleMatcher::new("|", Type::VerticalBar).into(),
			SimpleMatcher::new(".", Type::Dot).into(),
			SimpleMatcher::new(",", Type::Comma).into(),
			SimpleMatcher::new(";", Type::Semicolon).into(),
			SimpleMatcher::new(":", Type::Colon).into(),
			SimpleMatcher::new("?", Type::QuestionMark).into(),
			SimpleMatcher::new("!", Type::ExclamationMark).into(),
		];

		let extra_string_matchers: Vec<Matchers> = vec![
			StringMatcher::new(']', '"', Type::InterpolatedStringRight).into(),
			StringMatcher::new(']', '[', Type::InterpolatedStringMiddle).into(),
		];
		
		lexer.run(matchers, extra_string_matchers);

		(lexer.tokens, if lexer.errors.is_empty() {
			None
		} else {
			Some(Errors {
				message: format!("Found {} error(s) while tokenizing", lexer.errors.len()),
				errors: lexer.errors
			})
		})
	}

	fn next(&mut self) -> Option<char> {
		self.column += 1;
		return self.chars.next().map(|pair| pair.1);
	}

	fn peek_idx(&mut self) -> usize {
		self.chars.peek().map(|(idx, _c)| *idx).unwrap_or(self.code.len())
	}

	/// The last matching matcher is prioritized
	fn run(
		&mut self,
		mut token_matchers: Vec<Matchers>,
		mut extra_string_matchers: Vec<Matchers>,
	) {
		let mut parenth_depth = 0;
		let mut string_depth = 0;
		loop {
			let start_idx = if let Some((start_idx, _)) = self.chars.peek() {
				*start_idx
			} else {
				break;
			};
			let matchers = if string_depth > 0 {
				token_matchers.iter_mut().chain(extra_string_matchers.iter_mut()).collect::<Vec<&mut Matchers>>()
			} else {
				token_matchers.iter_mut().collect::<Vec<&mut Matchers>>()
			};
			
			let result = self.next_token(start_idx, matchers);
			match result {
				Ok(mut token) => {
					match token.token_type {
						Type::InterpolatedStringLeft => {
							string_depth += 1;
							if string_depth > 1 {
								self.errors.push(token_error!(self, "Nested interpolated strings are not allowed! Try to de-nest the strings by doing the calculation in multiple rows instead of one."));
							}
							if parenth_depth == 0 {
								self.tokens.push(token)
							}
						}
						Type::InterpolatedStringRight => {
							string_depth -= 1;
							if string_depth < 0 {
								unreachable!()
							}
							if parenth_depth == 0 {
								self.tokens.push(token)
							}
						}
						Type::NewLine => {
							if string_depth > 0 {
								self.errors.push(token_error!(self, "Missing {} closing brackets ']\"' in interpolated string", string_depth));
								string_depth = 0;
							}

							if parenth_depth > 0 {
								self.errors.push(token_error!(self, "Missing {} closing parentheses ')'", parenth_depth));
								parenth_depth = 0;
							}
							token.column -= 1;
							self.row += 1;
							self.column = 1;
							self.tokens.push(token)
						}
						Type::ParenthesisOpen  => {
							parenth_depth += 1;
						}
						Type::ParenthesisClose => {
							parenth_depth -= 1;
							if parenth_depth < 0 {
								self.errors.push(token_error!(self, "Too many closing parentheses ')'"));
								parenth_depth = 0;
							}
						}
						Type::NonTabWhitespace => { /* Ignore */ }
						_ => if parenth_depth == 0 {
							self.tokens.push(token)
						}
					}
				}
				Err(token_string) => {
					self.column -= 1;
					self.errors.push(token_error!(self, "Did not know what to do with string '{}'", token_string));
					if token_string.ends_with('\n') {
						if string_depth > 0 {
							self.errors.push(token_error!(self, "Missing {} closing brackets ']\"' in interpolated string", string_depth));
							string_depth = 0;
						}

						if parenth_depth > 0 {
							self.errors.push(token_error!(self, "Missing {} closing parentheses ')'", parenth_depth));
							parenth_depth = 0;
						}
						self.tokens.push(Token {
							token_type: Type::NewLine,
							string: token_string,
							column: self.column
						});
						self.row += 1;
						self.column = 1;
					}
				}
			}
		}
	}
	
	fn next_token(&mut self, start_idx: usize, mut matchers: Vec<&mut Matchers>) -> Result<Token<'a>, &'a str> {
		let mut match_state = None;

		for matcher in matchers.iter_mut() {
			matcher.reset();
		}
		
		while let Some(character) = self.next() {
			let mut next_matchers = vec![];
			for matcher in matchers.into_iter() {
				match matcher.test(character) {
					Status::MatchContinue(token_type) => {
						match_state = Some((token_type, self.chars.clone(), self.column));
						next_matchers.push(matcher);
					},
					Status::MatchStop(token_type) => {
						match_state = Some((token_type, self.chars.clone(), self.column));
					},
					Status::Continue => {
						next_matchers.push(matcher);
					},
					Status::Stop => {
						/* Do nothing */
					},
				}
			}
			if next_matchers.is_empty() {
				break
			}
			matchers = next_matchers;
		}
		
		match_state.map(|(token_type, chars, column)| {
			self.chars = chars;
			self.column = column;
			let token_string = &self.code[start_idx..self.peek_idx()];

			Token {
				token_type: token_type,
				string: token_string,
				column: self.column
			}
		}).ok_or(&self.code[start_idx..self.peek_idx()])
	}	
}

#[cfg(test)]
fn fn_test_map_types(tokens: &Vec<Token>) -> Vec<Type> {
	tokens.iter().map(|token| token.token_type).collect()
}

#[cfg(test)]
fn fn_test_map_strings<'a>(tokens: &Vec<Token<'a>>) -> Vec<&'a str> {
	tokens.iter().map(|token| token.string).collect()
}

#[test]
fn test_lexer_basic() {
	let tokens = Lexer::tokenize_cookiean("\thello \"string\" 123\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![Type::Tabs, Type::Word, Type::StringLiteral, Type::Integral, Type::NewLine]
	);
	assert_eq!(
		fn_test_map_strings(&tokens),
		vec!["\t", "hello", "\"string\"", "123", "\n"]
	)
}

#[test]
fn test_lexer_column_numbers() {
	assert_eq!(
		Lexer::tokenize_cookiean("me 1 0.1 0..1 \"test[ yep ]\" \"[a ][ b]\"\n").0.iter().map(|tok| tok.column).collect::<Vec<usize>>(),
		vec![3, 5, 9, 14, 21, 25, 28, 31, 32, 35, 37, 39, 39]
	);
}

#[test]
fn test_lexer_column_numbers_with_new_line() {
	assert_eq!(
		Lexer::tokenize_cookiean("0..1 yes\n\t2..3 no\n\"hello\"\n").0.iter().map(|tok| tok.column).collect::<Vec<usize>>(),
		vec![5, 9, 9, 2, 6, 9, 9, 8, 8]
	);
}

#[test]
fn test_lexer_unterminated_string() {
	let (_tokens, errors) = Lexer::tokenize_cookiean("\"hahaa\n");
	assert!(errors.is_some(), "Did not get any errors on unterminated string");
	assert_eq!(errors.unwrap().errors.len(), 1);
}

#[test]
fn test_lexer_basics() {
	let tokens = Lexer::tokenize_cookiean("\"hi\" is:\n\treversed\n\tdisplayed\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![Type::StringLiteral, Type::Is, Type::Colon, Type::NewLine, Type::Tabs, Type::Word, Type::NewLine, Type::Tabs, Type::Word, Type::NewLine]
	)
}

#[test]
fn test_lexer_newline() {
	let tokens = Lexer::tokenize_cookiean("\n\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![Type::NewLine, Type::NewLine]
	)
}

#[test]
fn test_lexer_more_stuff() {
	let tokens = Lexer::tokenize_cookiean("(\"this is an annotated string\")\nhi!\n\t10.0.0.1\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![Type::NewLine, Type::Word, Type::ExclamationMark, Type::NewLine, Type::Tabs, Type::DotSequence, Type::NewLine]
	)
}

#[test]
fn test_lexer_ordinals() {
	let tokens = Lexer::tokenize_cookiean("C# is 1\nRust is #1\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![Type::Word, Type::Is, Type::Integral, Type::NewLine, Type::Word, Type::Is, Type::Ordinal, Type::NewLine]
	)
}

#[test]
fn test_lexer_interpolated_string() {
	let tokens = Lexer::tokenize_cookiean("[p] greeted does \"Hello [name of p]!\"\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![Type::BracketOpen, Type::Word, Type::BracketClose, Type::Word, Type::Does, Type::InterpolatedStringLeft, Type::Word, Type::Of, Type::Word, Type::InterpolatedStringRight, Type::NewLine]
	)
}

#[test]
fn test_lexer_basic_integer_sequences() {
	let tokens = Lexer::tokenize_cookiean("0.0 0.0.1\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![Type::DotSequence, Type::DotSequence, Type::NewLine]
	)
}

#[test]
fn test_lexer_broken_integer_sequences() {
	let tokens = Lexer::tokenize_cookiean("0.1.a.b,c.2.3.d,e.4.f.5\n").0;
	assert_eq!(
		fn_test_map_types(&tokens), //should these be legal?
		vec![
			Type::DotSequence, Type::Comma, Type::DotSequence, Type::Comma, Type::DotSequence, Type::NewLine
		]
	)
}

#[test]
fn test_lexer_sequences() {
	let tokens = Lexer::tokenize_cookiean("3. 0:1.2 a: b c.d:e\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![
			Type::Integral, Type::Dot, Type::ColonSequence, Type::DotSequence, Type::A, Type::Colon,
			Type::Word, Type::DotSequence, Type::ColonSequence, Type::NewLine
		]
	)
}

#[test]
fn test_lexer_nums_and_strs() {
	let tokens = Lexer::tokenize_cookiean("00300\"Helsinki\"12\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![
			Type::Integral, Type::StringLiteral, Type::Integral, Type::NewLine
		]
	)
}

#[test]
fn test_lexer_consecutive_strings() {
	let tokens = Lexer::tokenize_cookiean("\"a[\"b\"]c[]d\"\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![
			Type::InterpolatedStringLeft, Type::StringLiteral, Type::InterpolatedStringMiddle, Type::InterpolatedStringRight, Type::NewLine
		]
	)
}

#[test]
fn test_lexer_ratios() {
	let tokens = Lexer::tokenize_cookiean("# of apples per bag is 12:1\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![
			Type::Word, Type::Of, Type::Word, Type::Word, Type::Word, Type::Is, Type::ColonSequence, Type::NewLine
		]
	)
}

#[test]
fn test_lexer_three_quotes_error_row_numbers() {
	let errors = Lexer::tokenize_cookiean("\"\n\"\n\"\n").1;
	assert!(errors.is_some(), "Expected errors but got none");
	let err_rows = errors.unwrap().errors.iter().map(|err| err.row).collect::<Vec<_>>();
	assert_eq!(err_rows, vec![1,2,3]);
}

#[test]
fn test_lexer_interpolated_string_2() {
	let tokens = Lexer::tokenize_cookiean("\"Hello [name of Waffe]!\"\n\"A word\" between \"strings.\"\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![
			Type::InterpolatedStringLeft, Type::Word, Type::Of, Type::Word, Type::InterpolatedStringRight, Type::NewLine,
			Type::StringLiteral, Type::Word, Type::StringLiteral, Type::NewLine
		]
	)
}

#[test]
fn test_lexer_interpolated_string_3() {
	let tokens = Lexer::tokenize_cookiean("[x] y \"z\"").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![
			Type::BracketOpen, Type::Word, Type::BracketClose, Type::Word, Type::StringLiteral
		]
	)
}

#[test]
fn test_lexer_unicode() {
	let (tokens, errors) = Lexer::tokenize_cookiean("
师傅 is a master
Mężny bądź, chroń pułk twój i sześć flag is a pangram
and so is Мы любим грамматику
");
	assert!(errors.is_none(), "Unexpected errors {:?}", errors);
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![
			Type::NewLine,
			Type::Word, Type::Is, Type::A, Type::Word, Type::NewLine,
			Type::Word, Type::Word, Type::Comma, Type::Word, Type::Word, Type::Word, Type::Word, Type::Word, Type::Word, Type::Is, Type::A, Type::Word, Type::NewLine,
			Type::Word, Type::Word, Type::Is, Type::Word, Type::Word, Type::Word, Type::NewLine
		]
	)
}

#[test]
fn test_lexer_interpolated_string_comment() {
	let (tokens, errors) = Lexer::tokenize_cookiean("(\"test [test] test\")\n");
	assert!(errors.is_none(), "Unexpected errors {:?}", errors);
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![Type::NewLine]
	)
}

#[test]
fn test_lexer_all_tokens() {
	let tokens = Lexer::tokenize_cookiean("is.?does,do:!defines;has \t\n\tas()[]{}\" [\"Hi\"] [] \"123.12 of a an some animal |\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![
			Type::Is, Type::Dot, Type::QuestionMark, Type::Does, Type::Comma, Type::Do, Type::Colon, Type::ExclamationMark, Type::Word, Type::Semicolon, Type::Has,
			Type::TabsMixedWhitespace, Type::NewLine, Type::Tabs, Type::Word, Type::BracketOpen,
			Type::BracketClose, Type::BraceOpen, Type::BraceClose, Type::InterpolatedStringLeft, Type::StringLiteral,
			Type::InterpolatedStringMiddle, Type::InterpolatedStringRight, Type::DotSequence, Type::Of, Type::A, Type::A, Type::A, Type::Word, Type::VerticalBar, Type::NewLine
		]
	)
}

#[test]
fn test_lexer_negative_numbers() {
	let tokens = Lexer::tokenize_cookiean("-1 -2.0 -3.-4 -5:-6\n").0;
	assert_eq!(
		fn_test_map_types(&tokens),
		vec![Type::Integral, Type::DotSequence, Type::DotSequence, Type::ColonSequence, Type::NewLine]
	);
	assert_eq!(
		fn_test_map_strings(&tokens),
		vec!["-1", "-2.0", "-3.-4", "-5:-6", "\n"]
	)
}