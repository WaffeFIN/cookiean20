
use std::fmt;

/// A single 'word' of Cookiean
#[derive(Clone, Debug, PartialEq)]
pub struct Token<'a> {
	pub token_type: Type,
	pub string: &'a str,
	pub column: usize
}

/// Token<'a> type
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Type {
	// Whitespace
	Tabs,
	TabsMixedWhitespace, // allowed only if empty line
	NonTabWhitespace,
	NewLine,
	// Punctuation
	Colon,
	QuestionMark,
	ParenthesisOpen,
	ParenthesisClose,
	BracketOpen,
	BracketClose,
	BraceOpen,
	BraceClose,
	Dot,
	Comma,
	Semicolon,
	ExclamationMark,
	VerticalBar,
	// Verbs
	Is,
	Does,
	Done,
	Do,
	Has,
	Uses,
	Use,
	Return,
	// Values
	StringLiteral,
	InterpolatedStringLeft,
	InterpolatedStringMiddle,
	InterpolatedStringRight,
	Integral,
	Ordinal,
	DotSequence,
	ColonSequence,
	Word,
	Yes,
	No,
	// Keywords
	Of,
	By,
	The,
	A,
	With,
}
impl Type {
	pub fn is_verb(&self) -> bool {
		use self::Type as T;
		match self {
			T::Is | T::Does | T::Do | T::Use | T::Uses | T::Has | T::Done | T::Return => true,
			_ => false
		}
	}
	
	/// Return true if the token type can be used as part of an identifier/expression
	pub fn is_expr(&self) -> bool {
		use self::Type as T;
		match self {
			T::TabsMixedWhitespace | T::Tabs | T::NewLine |
			T::BracketOpen | T::BracketClose | T::Colon | T::VerticalBar => false,
			_ => !self.is_verb()
		}
	}

	pub fn is_separator(&self) -> bool {
		use self::Type as T;
		match self {
			T::Comma | T::Semicolon => true,
			_ => false
		}
	}
}

/// Groups errors together from one step
pub struct Errors {
	pub message: String,
	pub errors: Vec<Error>
}
impl fmt::Display for Errors {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		let stringified_errors: Vec<String> = self.errors.iter().map(|error| error.to_string()).collect();
		write!(f, "{}:\n#\t{}", self.message, stringified_errors.join("\n\n#\t"))
	}
}
impl fmt::Debug for Errors {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		let stringified_errors: Vec<String> = self.errors.iter().map(|error| error.to_string()).collect();
		write!(f, "{}:\n#\t{}", self.message, stringified_errors.join("\n\n#\t"))
	}
}

pub fn stringify_errors(errors_vec: Vec<Option<Errors>>) -> (String, usize) {
	let mut error_message = "".to_string();
	let mut sum: usize = 0;
	for errors_opt in errors_vec {
		if let Some(errors) = errors_opt {
			error_message = format!("{}\n\n{}", error_message, errors);
			sum += errors.errors.len();
		}
	}
	(error_message, sum)
}

/// A generic static code error
pub struct Error {
	pub error_type: String,
	pub message: String,
	pub row: usize,
	pub column: usize
}

impl fmt::Display for Error {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{} error at row {} column {}: {}", self.error_type, self.row, self.column, self.message)
	}
}
impl fmt::Debug for Error {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{} error at row {} column {}: {}", self.error_type, self.row, self.column, self.message)
	}
}
