extern crate baker;
extern crate clap;
extern crate git_version;

use std::fs;
use std::io;
use baker::tokenizing::lexer::Lexer;
use clap::{App, Arg, arg};
use git_version::git_version;

const BUILD_COMMIT: &str = git_version!();
const RELEASE: &str = env!("CARGO_PKG_VERSION");

fn main() {
	// parse command-line arguments & options
	let version = &*format!("{} build {}", RELEASE, BUILD_COMMIT);
	let matches = App::new("Baker")
		.version(version)
		.author("Walter Grönholm <waltergronholm@gmail.com>")
		.about("Interpreter for Cookiean code (.qki)")
		.arg(arg!(-i --in  [FILE]   "Sets a file to be used as user input. By default the console is used"))
		.arg(arg!(-o --out [FILE]   "Sets a file to be used for program output. By default the console is used"))
		.arg(arg!(-T --tokens       "Debug print the tokens before parsing"))
		.arg(arg!(-R --rows         "Debug print the resulting rows"))
		.arg(arg!(-A --ast          "Debug print the resulting abstract syntaxt tree in depth-first pre-order"))
		.arg(arg!(-C --crumbs       "Debug print the generated instructions/Crumb<'a>s"))
		.arg(arg!(-I --instructions "Debug print each instruction run"))
		.arg(arg!(-v --version      "Display the program build version (without running the program)"))
		.arg(Arg::new("no-run")
			.long("no-run")
			.help("Crumb<'a>le the code but do not run it"))
		.arg(Arg::new("file")
			.required_unless_present("version")
			.takes_value(true)
			.help("Code file to interpret"))
		.get_matches();
	
	if matches.is_present("version") {
		println!("Baker {}", version);
		return
	}
	
	let code_file = matches.value_of("file").unwrap();
	let code = match fs::read_to_string(code_file) {
		Ok(code) => code,
		Err(msg) => panic!("Could not read file {:?}: {:?}", code_file, msg)
	};

	let stdin = io::stdin();
	let mut input: Box<dyn io::BufRead> = matches.value_of("in").map(
		|path| Box::new(io::BufReader::new(fs::File::open(path).expect("Could not read input file"))) as Box<dyn io::BufRead>
	).unwrap_or(
		Box::new(stdin.lock())
	);
	let mut output: Box<dyn io::Write> = matches.value_of("out").map(
		|path| Box::new(fs::File::create(path).expect("Could not create output file")) as Box<dyn io::Write>
	).unwrap_or(
		Box::new(io::stdout())
	);
	run(code, &matches, &mut *input, &mut *output);
}

// todo use Box<dyn ...>?
fn run(mut code: String, matches: &clap::ArgMatches, std_in: &mut dyn io::BufRead, std_out: &mut dyn io::Write) {
	code.push('\n');
	let code = &code[..];
	let (tokens, lexer_errors) = Lexer::tokenize_cookiean(code);
	if matches.is_present("tokens") { debug_print(&tokens); }
	let (rows, row_parser_errors) = baker::parsing::row_parser::parse(tokens);
	if matches.is_present("rows") { debug_print(&rows); }
	let (ast_root, block_parser_errors) = baker::parsing::block_parser::parse(rows);
	if matches.is_present("ast") { println!("{}", baker::parsing::block_parser::debug_stringify_tree(&ast_root, 0)); }

	let (msg, n_errs) = baker::tokenizing::structs::stringify_errors(vec![block_parser_errors, row_parser_errors, lexer_errors]);

	if n_errs > 0 {
		panic!("{}\n\nFound {} error(s)", msg, n_errs)
	}

	//let mut symbol_table = baker::analyzing::symbol_table::SymbolTable::new();
	//let scope_errors     = baker::analyzing::scope_analyzer::analyze(&ast_root, &mut symbol_table);
	//let type_errors      = baker::analyzing::type_analyzer::analyze(&ast_root, &mut symbol_table);

	//handle_errors(vec![type_errors, scope_errors]);

	let instructions = Vec::from(ast_root);
	if matches.is_present("crumbs") { debug_print(&instructions); }
	if matches.is_present("no-run") { return }
	baker::interpreting::crumbs_interpreter::run(instructions, std_in, std_out, matches.is_present("instructions"));
}

fn debug_print<T: std::fmt::Debug>(stuff: &Vec<T>) {
	let mut iteration = stuff.iter();
	while let Some(thingy) = iteration.next() {
		println!("{:?}", thingy);
	}
	println!("------------------------------");
}