
extern crate num_rational;
use self::num_rational::Rational64;

use crate::tokenizing::structs::{Token, Type};
use crate::parsing::structs::{Node, StatementRowStructure};
use std::rc::Rc;
use std::borrow::Cow;

const SEPARATOR_MAX: usize = 0;
const SEPARATOR_SEMICOLON: usize = 1;
const SEPARATOR_COMMA: usize = 2;
const N_SEPARATORS: usize = 3;

#[derive(Clone, PartialEq, Eq, Debug, Hash)]
pub enum Key<'a> {
	Ordinal(usize),
	Fraction(Rational64),
	Boolean(bool),
	Text(Cow<'a, str>), // TODO could manually impl Hash for faster hashing
	Word(&'a str),      // TODO could manually impl Hash or use Word dict for faster hashing + this can be used to alias a/an/some
}
impl<'a> std::fmt::Display for Key<'a> {

	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		use self::Key as K;
		match self {
			K::Ordinal(nth)        => write!(f, "#{}", nth + 1),
			K::Fraction(q)         => write!(f, "{:.6}", q),
			K::Boolean(b)          => write!(f, "{}", if *b {"yes"} else {"no"}),
			K::Text(s)             => write!(f, "{}", s),
			K::Word(w)             => write!(f, "'{}'", w)
		}
	}
}

const STR_TRIM_CHARS: &[char] = &['"', '[', ']'];
const ORD_TRIM_CHAR:  &[char] = &['#'];

impl<'a> TryFrom<Token<'a>> for Key<'a> {
	type Error = Type;

	fn try_from(token: Token<'a>) -> Result<Key<'a>, Self::Error> {
		use self::Type as T;
		Ok(match token.token_type {
			T::Integral         => Key::Fraction(Rational64::from(token.string.parse::<i64>().unwrap())),
			T::Ordinal          => Key::Ordinal(token.string.trim_start_matches(ORD_TRIM_CHAR).parse::<usize>().unwrap() - 1),
			T::InterpolatedStringLeft   |
			T::InterpolatedStringMiddle |
			T::InterpolatedStringRight  |
			T::StringLiteral    => Key::Text(token.string.trim_matches(STR_TRIM_CHARS).into()),
			T::Yes              => Key::Boolean(true),
			T::No               => Key::Boolean(false),
			T::A               |
			T::Dot             |
			T::ExclamationMark |
			T::QuestionMark    |
			T::Word            => Key::Word(token.string.into()),
			T::DotSequence => {
				// TODO handle 3+ item sequences
				let mut nums: Vec<i64> = vec![];
				let mut lens: Vec<usize> = vec![];
				for part in token.string.split('.') {
					lens.push(part.chars().count());
					nums.push(part.parse().unwrap())
				}
				let denominator = 10i64.pow(lens[1] as u32);
				Key::Fraction(Rational64::new(nums[0] * denominator + nums[1], denominator))
			}
			T::ColonSequence => {
				// TODO handle 3+ item sequences
				let mut nums: Vec<i64> = vec![];
				for part in token.string.split(':') {
					nums.push(part.parse().unwrap())
				}
				Key::Fraction(Rational64::new(nums[0], nums[1]))
			}
			T::Return |
			T::VerticalBar | T::Tabs | T::TabsMixedWhitespace | T::NonTabWhitespace | T::NewLine |
			T::Colon | T::Comma | T::Semicolon |
			T::ParenthesisClose | T::ParenthesisOpen |
			T::BracketOpen | T::BracketClose |
			T::BraceOpen | T::BraceClose |
			T::Is | T::Does | T::Do | T::Uses | T::Use | T::Done | T::Has |
			T::The | T::Of | T::By | T::With => return Err(token.token_type)
		})
	}
}

/// An instruction for the stack-based intermediate language Crumbs.
// Current size: (3 x usize) bytes per Crumb. Size cap due to Rc. TODO: Use strong-only verson of Rc
// Target size:  (2 x usize) bytes per Crumb
#[derive(Clone, Debug)]
pub enum Crumb<'a> {
	Enter {                 // Get or create, then push result onto object stack
		keys: Rc<[Key<'a>]> // keys are in order
	},
	EnterBeneath {          // Same as enter, but first pops the current object and lastly pushes it back
		keys: Rc<[Key<'a>]> // keys are in order
	},
	PopEnter,               // Pop current object (which must be a key) and enter into it 
	Discard {               // Clears eval stack and reverts 'exits' Enters
		exits: usize
	},
	PreEval {               // Pushes segment and sets row and column
		row: usize,
		column: usize
	},
	Find {                          // Build object stack by finding values by keys
		reverse_keys: Rc<[Key<'a>]> // keys are in reverse order (pop order)
	},
	PopFind {                       // Pop current object and continue find
		reverse_keys: Rc<[Key<'a>]> // keys are in reverse order (pop order
	},
	Separator {             // Used as a marker, both for column index and for modifiers on separators
		n_crumbs: usize,    // In case of this crumb being the input to a modifier, grab this many crumbs instead
		column: usize
	},
	Eval {                  // Squashes eval stack into a single item. Sets row and column
		row: usize,
		column: usize
	},
	Meld {                  // Melds the top most eval stack item into the current object
		exits: usize
	},
	EnterContext,           // Enter the current object's context
	Binding {               // Assigns a yet-to-be-bound self reference to the current object
		back: usize,        // 'back' is essentially the word count of the method name
		name: Rc<[Key<'a>]> // 'name' is the name of the binding (e.g. 'me')
	},
	InlineFunction(Rc<[Crumb<'a>]>), // push function to object stack. Used by 'return' and 'do'
	Function(Rc<[Crumb<'a>]>),       // push function to object stack. Functions are called on Eval
	InlineGetter(Rc<[Crumb<'a>]>),   // push method to object stack. Getters are called on Find
	Getter(Rc<[Crumb<'a>]>),         // push method to object stack. Getters are called on Find
	ModifierFn(Rc<[Crumb<'a>]>),     // push modifier to object stack. Modifiers are called on Find and/or Eval
	ModifierOp(Rc<[Crumb<'a>]>),     // push modifier to object stack. Uses the right expression
	Return,
	OutOfCall {                      // Marks the end of the current call. Pops segment keeping current object
		row: usize,
		column: usize
	},
	OutOfModifier {         // Marks the end of the current modifier call.
		row: usize,         // Drains current segment, pops two segments, pops stored value and appends drained segment and popped value
		column: usize
	},
	Bind {                  // Only used by crumbs_interpreter. Circumvents an issue with modifiers
		modifier: crate::interpreting::structs::Object<'a>,
		into: usize
	}
	// HotLoad crumb?
}
fn keyfy(token: Token) -> Key {
	match Key::try_from(token) {
		Ok(key) => key,
		Err(token_type) => {
			panic!("Unexpected token type '{:?}' is keyfied", token_type);
		}
	}
}
fn keyfy_all(tokens: Vec<Token>) -> Vec<Key> {
	tokens.into_iter().rev()
			.filter(|tok| tok.token_type != Type::Of
					&& tok.token_type != Type::By
					&& tok.token_type != Type::The
				    && tok.token_type != Type::With
			)
			.map(|tok| keyfy(tok))
			.collect()
}

impl<'a> Crumb<'a> {
	fn enter(tokens: Vec<Token<'a>>) -> (Crumb<'a>, usize) {
		let keys: Rc<[Key<'a>]> = keyfy_all(tokens).into();
		let length = keys.len();
		(Crumb::Enter{ keys: keys }, length)
	}

	fn assign(row: usize, keys: Vec<Token<'a>>, expression: Vec<Token<'a>>) -> (Vec<Crumb<'a>>, usize) {
		let column = expression.last().unwrap().column;
		let mut crumbs = Crumb::find(expression);
		crumbs.push(Crumb::PreEval { row, column });
		crumbs.reverse();
		let keys: Rc<[Key<'a>]> = keyfy_all(keys).into(); // TODO: enter_include_inline, InlineGetter from interp strs
		let exits = keys.len();
		crumbs.push(Crumb::Eval { row, column });
		crumbs.push(Crumb::EnterBeneath { keys: keys });
		crumbs.push(Crumb::Meld { exits: 0 });
		(crumbs, exits)
	}

	fn find_inner(columns: &[usize; N_SEPARATORS], result: &mut [Vec<Crumb<'a>>; N_SEPARATORS], keys: Vec<Key<'a>>, current_separator: usize, next_separator: usize) {
		if !keys.is_empty() {
			result[current_separator].push(Crumb::Find {
				reverse_keys: keys.into()
			});
		}
		
		let mut i = N_SEPARATORS;
		while i > next_separator {
			i -= 1;
			let crumbs = std::mem::replace(&mut result[i], Vec::new());
			if crumbs.is_empty() {
				continue
			}
			
			if let &Crumb::Separator { .. } = crumbs.first().unwrap(/* Asserted not empty */) {
				/* Do not chain separators */
			} else {
				result[i - 1].push(Crumb::Separator { n_crumbs: crumbs.len(), column: columns[i] });
			}
			result[i - 1].extend(crumbs);
		}
	}

	/// This method could use a bit of refactoring
	/// 
	/// Creates a series of crumbs like so: PreFind Find (Separator Find)+
	fn find(tokens: Vec<Token<'a>>) -> Vec<Crumb<'a>> {
		if tokens.is_empty() {
			return vec![]
		}

		let mut current_separator = SEPARATOR_MAX;
		let mut result: [Vec<Crumb<'a>>; N_SEPARATORS] = [vec![], vec![], vec![]];
		let column = tokens.last().unwrap().column;
		let mut columns: [usize; N_SEPARATORS] = [column, column, column];

		let mut keys = vec![];
		for token in tokens.into_iter().rev() {
			match token.token_type {
				Type::Of | Type::By | Type::The | Type::With => { /* Skip */ }
				Type::Comma => {
					keys.reverse();
					Crumb::find_inner(
						&columns,
						&mut result,
						keys, 
						current_separator,
						SEPARATOR_COMMA
					);
					current_separator = SEPARATOR_COMMA;
					keys = vec![];
					columns[current_separator] = 0;
				}
				Type::Semicolon => {
					keys.reverse();
					Crumb::find_inner(
						&columns,
						&mut result,
						keys, 
						current_separator,
						SEPARATOR_SEMICOLON
					);
					current_separator = SEPARATOR_SEMICOLON;
					keys = vec![];
					columns[current_separator] = 0;
				}
				_ => {
					if columns[current_separator] == 0 {
						columns[current_separator] = token.column;
					}
					keys.push(keyfy(token));
				}
			}
		}
		keys.reverse();
		Crumb::find_inner(
			&columns,
			&mut result,
			keys, 
			current_separator,
			SEPARATOR_SEMICOLON
		);
		let mut result = std::mem::replace(&mut result[SEPARATOR_MAX], vec![]);
		result.reverse();
		result
	}
}

const CONCATENATION: Key<'static> = Key::Word("&");
const TEMP_KEY: Key<'static> = Key::Word(".temp");

fn column_of(expression: &Vec<Token<'_>>) -> usize {
	expression.last().unwrap().column
}

fn eval_interpolated_string<'a>(row: usize, left_string: Token<'a>, tokens: &mut std::vec::IntoIter<Token<'a>>) -> Vec<Crumb<'a>> {
	let mut keys: Vec<Token<'a>> = vec![];
	let mut concat_expression: Vec<Key<'a>> = vec![keyfy(left_string)];
	let mut method_crumbs = vec![];

	let mut temp_vals = 0;
	
	while let Some(token) = tokens.next() {
		match token.token_type {
			Type::The  |
			Type::Of   |
			Type::With |
			Type::By   => continue,
			Type::InterpolatedStringMiddle => {
				let column = keys.last().unwrap().column;
				let key = Key::Ordinal(temp_vals);
				method_crumbs.push(Crumb::Enter{ keys: Rc::new([TEMP_KEY.clone(), key.clone()]) });
				method_crumbs.push(Crumb::PreEval { row, column });
				method_crumbs.extend(Crumb::find(std::mem::take(&mut keys)));
				method_crumbs.push(Crumb::Eval { row, column });
				method_crumbs.push(Crumb::Meld { exits: 2 });
				temp_vals += 1;
				concat_expression.push(CONCATENATION.clone());
				concat_expression.push(key);
				concat_expression.push(TEMP_KEY.clone());
				concat_expression.push(CONCATENATION.clone());
				concat_expression.push(keyfy(token));
			}
			Type::InterpolatedStringRight => {
				let column = keys.last().unwrap().column;
				let key = Key::Ordinal(temp_vals);
				method_crumbs.push(Crumb::Enter{ keys: Rc::new([TEMP_KEY.clone(), key.clone()]) });
				method_crumbs.push(Crumb::PreEval { row, column });
				method_crumbs.extend(Crumb::find(std::mem::take(&mut keys)));
				method_crumbs.push(Crumb::Eval { row, column });
				method_crumbs.push(Crumb::Meld { exits: 2 });

				concat_expression.push(CONCATENATION.clone());
				concat_expression.push(key);
				concat_expression.push(TEMP_KEY.clone());
				concat_expression.push(CONCATENATION.clone());
				concat_expression.push(keyfy(token));
				method_crumbs.push(Crumb::PreEval { row, column });
				method_crumbs.push(Crumb::Find { reverse_keys: concat_expression.into() });
				method_crumbs.push(Crumb::Eval { row, column });
				method_crumbs.push(Crumb::Meld { exits: 0 });
				method_crumbs.reverse();
				return method_crumbs
			}
			_ => {
				keys.push(token)
			}
		}
	}
	unreachable!();
}

fn find_include_inline<'a>(row: usize, tokens: Vec<Token<'a>>) -> Vec<Crumb<'a>> {
	let mut keys: Vec<Token<'a>> = vec![];
	let mut reverse_expression = vec![];

	let mut iterator = tokens.into_iter();
	while let Some(token) = iterator.next() {
		match token.token_type {
			Type::The  |
			Type::Of   |
			Type::With |
			Type::By   => continue,
			Type::InterpolatedStringLeft => {
				reverse_expression.extend(Crumb::find(std::mem::take(&mut keys)));
				if reverse_expression.is_empty() {
					reverse_expression.push(Crumb::PopFind { // run InlineGetter
						reverse_keys: vec![].into()
					});
				} else {
					let last_idx = reverse_expression.len() - 1;
					if let Crumb::Find { reverse_keys: keys } = &reverse_expression[last_idx] {
						reverse_expression[last_idx] = Crumb::PopFind { // run InlineGetter
							reverse_keys: keys.clone()
						}
					}
				}
				let getter_crumbs = eval_interpolated_string(row, token, &mut iterator);
				reverse_expression.push(Crumb::InlineGetter(
					getter_crumbs.into()
				));
			}
			_ => {
				keys.push(token)
			}
		}
	}
	
	reverse_expression.extend(Crumb::find(std::mem::take(&mut keys)));
	reverse_expression.reverse();
	return reverse_expression
}

fn push_function_or_operator<'a>(crumbs: &mut Vec<Crumb<'a>>, inner_crumbs: Rc<[Crumb<'a>]>, self_reference: Option<(usize, Vec<Token<'a>>)>) {
	let mut getter_crumbs = vec![];
	getter_crumbs.push(Crumb::Function(inner_crumbs));
	if let Some((back, self_reference)) = self_reference {
		getter_crumbs.push(Crumb::Binding {
			back: back,
			name: keyfy_all(self_reference).into()
		});
	}
	crumbs.extend(getter_crumbs);
}

fn exit_more(crumbs: &mut Vec<Crumb>, more_exits: usize) {
	if more_exits == 0 { return }
	let crumb = match crumbs.pop().unwrap() {
		Crumb::Discard { exits } => Crumb::Discard { exits: exits + more_exits },
		Crumb::Meld { exits } => Crumb::Meld { exits: exits + more_exits },
		other => {
			crumbs.push(Crumb::Discard { exits: more_exits });
			other
		}
	};
	crumbs.push(crumb);
}

fn generate_all<'a>(nodes: Vec<Node<'a>>) -> Vec<Crumb<'a>> {
	let crumbs = nodes.into_iter().flat_map(|node| Vec::from(node)).collect::<Vec<Crumb<'a>>>();
	crumbs
}

impl<'a> From<Node<'a>> for Vec<Crumb<'a>> {
	fn from(node: Node<'a>) -> Vec<Crumb<'a>> {
		use self::Node as N;
		let mut crumbs: Vec<Crumb<'a>> = vec![];
		match node {
			N::Root { children } => {
				crumbs.extend(generate_all(children));
				crumbs.reverse();
			}
			N::Is { row, data: StatementRowStructure { parameter: None, variable_name, self_reference: None }, children } => {
				// Assignment-like 'is'
				let (left_crumbs, exits) = Crumb::assign(row, variable_name.clone(), variable_name);

				crumbs.extend(left_crumbs);
				crumbs.extend(generate_all(children));
				exit_more(&mut crumbs, exits);
			}
			N::Is { row: _, data: StatementRowStructure { parameter: None, variable_name, self_reference: Some(self_reference) }, children } => {
				// Getter-like 'is'
				let mut inner_crumbs: Vec<Crumb<'a>> = vec![];
				
				// TODO Bind self reference
				inner_crumbs.extend(generate_all(children));
				inner_crumbs.reverse();
	
				let (enter, exits) = Crumb::enter(variable_name);
				crumbs.push(enter);
				crumbs.push(Crumb::Getter(inner_crumbs.into()));
				crumbs.push(Crumb::Binding { back: exits, name: keyfy_all(self_reference).into() });
				crumbs.push(Crumb::Meld { exits: exits });
			}
			N::Is { row: _, data: StatementRowStructure { parameter: Some(parameter), variable_name, self_reference }, children } => {
				// Constructor-like 'is'
				let mut inner_crumbs: Vec<Crumb<'a>> = vec![];
				// Since we always Eval into a function call, we know there's at least
				// one object ontop of the eval stack, which we can Eval into the param
				let keys = keyfy_all(parameter);
				let exits = keys.len();
				inner_crumbs.push(Crumb::EnterBeneath {
					keys: keys.into()
				});
				inner_crumbs.push(Crumb::Meld {
					exits: exits
				});

				inner_crumbs.extend(generate_all(children));
				inner_crumbs.reverse();
	
				let (enter, exits) = Crumb::enter(variable_name);
				crumbs.push(enter);
				push_function_or_operator(
					&mut crumbs, 
					inner_crumbs.into(), 
					self_reference.map(|tokens| (exits, tokens))
				);
				crumbs.push(Crumb::Meld { exits: exits });
			}
			N::Does { row: _, data: StatementRowStructure { parameter, variable_name, self_reference }, children } => {
				// Any named procedure, method, function or operator declaration
				let mut inner_crumbs: Vec<Crumb<'a>> = vec![];

				let has_parameter = parameter.is_some();
				if let Some(parameter) = parameter {
					let keys = keyfy_all(parameter);
					let exits = keys.len();
					inner_crumbs.push(Crumb::EnterBeneath {
						keys: keys.into()
					});
					inner_crumbs.push(Crumb::Meld {
						exits: exits
					});
				}

				let ends_in_expression = if let Some(N::Expression { .. }) = children.last() { true } else { false };
				inner_crumbs.extend(generate_all(children));
				if ends_in_expression {
					let last_crumb = inner_crumbs.pop();
					if let Some(Crumb::Meld { .. }) = last_crumb {
						/* Removing Meld avoids leaking function scope with the return value */
					} else {
						panic!("Expected a Meld crumb at the end of Expression, but found a {:?}", last_crumb);
					}			
				} else {
					// TODO: handle return
					panic!("Expected 'does' to end with Expression");
				}
				inner_crumbs.reverse();

				let (enter, exits) = Crumb::enter(variable_name);
				crumbs.push(enter);
				if has_parameter {
					push_function_or_operator(
						&mut crumbs,
						inner_crumbs.into(),
						self_reference.map(|tokens| (exits, tokens))
					);
				} else {
					crumbs.push(Crumb::Getter(inner_crumbs.into()));
					crumbs.push(Crumb::Binding {
						back: exits,
						name: keyfy_all(self_reference.unwrap_or(Vec::new())).into()
					});
				}
				crumbs.push(Crumb::Meld { exits });
			}
			N::Has { row, data: StatementRowStructure { parameter: None, variable_name, self_reference: None }, children } => {
				// Assignment-like 'has'
				let (left_crumbs, exits) = Crumb::assign(row, variable_name.clone(), variable_name);

				crumbs.extend(left_crumbs);
				crumbs.extend(generate_all(children));
				exit_more(&mut crumbs, exits);
			}
			N::Has { .. } => {
				// Type/structure definition
				todo!()
			}
			// Only available in 'use', 'uses' or 'has' blocks
			// a name
			// Becomes:
			// name is a name 
			// ...or...
			// name uses a name (TODO: later)
			N::AExpression{row, expression} => {
				let mut assign_keys = expression.clone();
				assign_keys.remove(0); // Remove leading a/an/some
				let (enter, exits) = Crumb::assign(row, assign_keys, expression);
				crumbs.extend(enter);
				exit_more(&mut crumbs, exits);
			}
			// Only available in 'use', 'uses' or 'has' blocks
			// first name of that person
			// Becomes:
			// first name is first name that person
			// ...or...
			// first name uses first name that person (TODO: later)
			N::OfExpression{ row, mut left, right } => {
				let assign_keys = left.clone();
				let expression = {left.extend(right); left};
				let (enter, exits) = Crumb::assign(row, assign_keys, expression);
				crumbs.extend(enter);
				exit_more(&mut crumbs, exits);
			}
			N::Do { row, expression, children } => {

				//increment_separators_to_include(&mut crumbs, eval_idx);
				let column = if let Some(expression) = expression {
					let Node::Expression { row, expression } = *expression else {
						panic!("Non expression {:?} as 'do's expression", expression);
					};
					let column = column_of(&expression);
					crumbs.push(Crumb::PreEval { row, column });
					crumbs.extend(find_include_inline(row, expression));
					column
				} else {
					let column = 0;
					crumbs.push(Crumb::PreEval { row, column });
					column
				};
				if !children.is_empty() {
					let mut inner_crumbs: Vec<Crumb<'a>> = vec![];
					inner_crumbs.extend(generate_all(children));
					inner_crumbs.reverse();
					inner_crumbs.push(Crumb::Discard { exits: 1 }); // Discard the pushed eval context
					crumbs.push(Crumb::InlineFunction(inner_crumbs.into()));
				}

				crumbs.push(Crumb::Eval { row, column });
				crumbs.push(Crumb::Meld { exits: 0 });
			}
			N::Done { row, function_parameter, right: modifier_name, this: right_expression, mut children } => {
				let mut inner_crumbs: Vec<Crumb<'a>> = vec![];
				// Left side will be bound in the modifier
				// The input is the right expression, if one is used
				let is_op = right_expression.is_some();
				if let Some(right_expression) = right_expression {
					let keys = keyfy_all(right_expression);
					let exits = keys.len();
					inner_crumbs.push(Crumb::EnterBeneath {
						keys: keys.into()
					});
					inner_crumbs.push(Crumb::Meld {
						exits: exits
					});
				}
				let last = children.pop();
				inner_crumbs.extend(generate_all(children));

				if let Some(N::Expression{row, expression: tokens}) = last {
					let column = tokens.last().unwrap().column;
					let last_crumbs = find_include_inline(row, tokens);
					inner_crumbs.push(Crumb::PreEval { row, column });
					inner_crumbs.extend(last_crumbs);
				} else {
					panic!("Modifier at row {} did not end in an expression", row)
				}
				inner_crumbs.reverse();
	
				let (enter, exits) = Crumb::enter(modifier_name);
				crumbs.push(enter);
				crumbs.push(
					if is_op {
						Crumb::ModifierOp(inner_crumbs.into())
					} else {
						Crumb::ModifierFn(inner_crumbs.into())
					}
				);
				crumbs.push(Crumb::Binding { back: 0, name: keyfy_all(function_parameter).into() });
				crumbs.push(Crumb::Meld { exits: exits });
				// TODO: 'done' must end with a single line 'return'?
			}
			N::Use { row: _, children } => {
				crumbs.push(Crumb::EnterContext);
				crumbs.extend(generate_all(children));
				exit_more(&mut crumbs, 1);
			}
			N::Uses { row: _, data: StatementRowStructure { parameter: None, variable_name, self_reference: None }, children } => {
				let (enter, exits) = Crumb::enter(variable_name);
				crumbs.push(enter);
				crumbs.push(Crumb::EnterContext);
				crumbs.extend(generate_all(children));
				exit_more(&mut crumbs, exits + 1);
			}
			N::Uses { .. } => {
				todo!()
				// push_foop(&mut crumbs, inner_crumbs.into(), self_reference);
			}
			N::Return { expression: None, children, .. } if children.is_empty() => {
				// Empty return should return nothing
				crumbs.push(Crumb::Find {
					reverse_keys: vec![Key::Word("nothing")].into()
				});
				crumbs.push(Crumb::Return);
			}
			N::Return { row, expression, children } => {
				let column = if let Some(expression) = expression {
					let Node::Expression { row, expression } = *expression else {
						panic!("Non expression {:?} as 'return's expression", expression);
					};
					let column = column_of(&expression);
					crumbs.push(Crumb::PreEval { row, column });
					crumbs.extend(find_include_inline(row, expression));
					column
				} else {
					let column = 0;
					crumbs.push(Crumb::PreEval { row, column });
					column
				};
				if !children.is_empty() {
					let mut inner_crumbs: Vec<Crumb<'a>> = vec![];
					inner_crumbs.extend(generate_all(children));
					inner_crumbs.reverse();
					inner_crumbs.push(Crumb::Discard { exits: 1 }); // Discard the pushed "function input"
					crumbs.push(Crumb::InlineFunction(inner_crumbs.into()));
				}
				crumbs.push(Crumb::Eval { row, column });
				crumbs.push(Crumb::Return);
				crumbs.push(Crumb::Discard { exits: 1 });
			}
			N::Expression { row, expression } => {
				let column = column_of(&expression);
				crumbs.push(Crumb::PreEval { row, column });
				crumbs.extend(find_include_inline(row, expression));
				crumbs.push(Crumb::Eval { row, column });
				crumbs.push(Crumb::Meld { exits: 0 });
			}
			N::Erroneous { row, children: _ } => {
				panic!("Tried to crumble erroneous row {}", row);
			}
		};
		crumbs
	}
}

/// Finds all separators that reach the crumb just before the inserted InlineFunction
/// Then increments their n_crumbs by one to include the InlineFunction
#[allow(dead_code)]
fn increment_separators_to_include<'a>(crumbs: &mut Vec<Crumb<'a>>, eval_idx: usize) {
	let mut separators_to_increment: Vec<(usize, Crumb<'a>)> = vec![];
	for (idx, crumb) in crumbs.iter().enumerate().rev() {
		match crumb {
			Crumb::PreEval { .. } => { break }
			Crumb::Separator { n_crumbs, column } => {
				if idx + n_crumbs == eval_idx - 1 {
					separators_to_increment.push((idx, Crumb::Separator {
						n_crumbs: n_crumbs + 1,
						column: *column
					}));
				}
			}
			_ => { continue }
		}
	}
	for (idx, separator) in separators_to_increment {
		crumbs[idx] = separator;
	}
}

// No unit tests