use crate::tokenizing::structs::{Token, Type};
use crate::tokenizing::structs::{Errors, Error};
use crate::parsing::row_parser::Row;
use crate::parsing::structs::*;

macro_rules! node_error {
	($parser:ident, $row_num:expr, $($arg:tt)*) => (
		Error { error_type: "Block parsing".to_string(), message: format!($($arg)*), row: $row_num, column: 0 }
	)
}

pub fn parse<'a>(rows: Vec<Row<'a>>) -> (Node<'a>, Option<Errors>) {
	BlockParser::parse(rows)
}

pub struct BlockParser {
	errors: Vec<Error>
}
impl BlockParser {
	
	pub fn parse<'a, T>(rows: T) -> (Node<'a>, Option<Errors>) where T: IntoIterator<Item = Row<'a>> {
		let mut block_parser = BlockParser { errors: vec![] };
		let root = block_parser.run(rows);
		let errors = if block_parser.errors.is_empty() { None } else {
			Some(Errors {
				message: format!("Found {} error(s) while parsing", block_parser.errors.len()),
				errors: block_parser.errors
			})
		};
		(root, errors)
	}

	fn run<'a, T>(&mut self, rows: T) -> Node<'a> where T: IntoIterator<Item = Row<'a>> {
		// Vec<(Parent node, Has special syntax)>
		let mut parents = vec![(Node::Root { children: vec![] }, false)];
		let mut block_was_started = true;

		for row in rows.into_iter() {
			let mut indentation = row.indentation;
			let max_indentation = parents.len() - 1;
			
			// Check indent
			if indentation > max_indentation {
				self.errors.push(node_error!(self, row.number, "Too many tabs! You need to remove at least {} tab(s), maybe more", indentation - max_indentation));
				indentation = max_indentation;
			}
			if indentation == max_indentation && !block_was_started {
				self.errors.push(node_error!(self, row.number, "Block started unexpectedly. Is there a colon ':' missing from the previous row?"));
			}
			if indentation < max_indentation && block_was_started {
				self.errors.push(node_error!(self, row.number, "Missing code block. Either remove the preceeding ':' or add indentation to this row"));
			}
			
			// Generate AST
			let (node, special_syntax) = match row.structure {
				RowStructure::IsStatement(statement, expressions)   => {
					block_was_started = expressions.block_starter.is_some();
					let children = Self::childrenify_expressions(row.number, expressions, Self::expressionify);
					(Node::Is   { row: row.number, data: statement, children: children }, false)
				}
				RowStructure::DoesStatement(statement, expressions) => {
					block_was_started = expressions.block_starter.is_some();
					let children = Self::childrenify_expressions(row.number, expressions, Self::expressionify);
					(Node::Does { row: row.number, data: statement, children: children }, false)
				}
				RowStructure::HasStatement(statement, expressions)  => {
					block_was_started = expressions.block_starter.is_some();
					let children = Self::childrenify_expressions(row.number, expressions, Self::expressionify_special);
					(Node::Has  { row: row.number, data: statement, children: children }, true)
				}
				RowStructure::UsesStatement(statement, expressions) => {
					block_was_started = expressions.block_starter.is_some();
					let children = Self::childrenify_expressions(row.number, expressions, Self::expressionify_special);
					(Node::Uses { row: row.number, data: statement, children: children }, true)
				}
				RowStructure::DoCommand(expressions) => {
					block_was_started = expressions.block_starter.is_some();
					let row_has_expression = expressions.expression.len() > 0;
					let mut children = Self::childrenify_expressions(row.number, expressions, Self::expressionify);
					let expression = if row_has_expression { Some(Box::new(children.remove(0))) } else { None };
					(Node::Do  { row: row.number, expression, children }, false)
				},
				RowStructure::UseCommand(expressions) => {
					block_was_started = expressions.block_starter.is_some();
					let children = Self::childrenify_expressions(row.number, expressions, Self::expressionify_special);
					(Node::Use { row: row.number, children: children }, true)
				},
				RowStructure::ReturnCommand(expressions) => {
					block_was_started = expressions.block_starter.is_some();
					let row_has_expression = expressions.expression.len() > 0;
					let mut children = Self::childrenify_expressions(row.number, expressions, Self::expressionify);
					let expression = if row_has_expression { Some(Box::new(children.remove(0))) } else { None };
					(Node::Return { row: row.number, expression, children }, false)
				},
				RowStructure::Done(done) => {
					block_was_started = true;
					let mut children = Vec::new();
					if done.block_expression.len() > 0 {
						children.push(Self::expressionify(row.number, done.block_expression))
					}
					(Node::Done { row: row.number, function_parameter: done.parameter, right: done.variable_name, this: done.self_reference, children: children }, false)
				},
				RowStructure::Expression(ExpressionRowStructure { expression }) => {
					block_was_started = false;
					let (_, special_syntax) = &parents[indentation];
					let expressionifier = if *special_syntax { Self::expressionify_special } else { Self::expressionify };
					(expressionifier(row.number, expression), false)
				},
				RowStructure::ErrorBlock => {
					block_was_started = true;
					(Node::Erroneous { row: row.number, children: Vec::new() }, false)
				}
			};
			
			self.collapse_to(&mut parents, indentation);
			parents.push((node, special_syntax));
		}
		if block_was_started {
			self.errors.push(node_error!(self, parents.last().unwrap().0.get_row(), "Missing code block. Either remove the ':' or add a code block after this row"));
		}
		self.collapse_to(&mut parents, 0);
		parents.pop().unwrap().0
	}

	fn collapse_to<'a>(&mut self, parents: &'a mut Vec<(Node<'_>, bool)>, indentation: usize) {
		let n_to_leave = indentation + 1;
		while parents.len() > n_to_leave {
			let (mut parent, _) = parents.pop().unwrap();
			let (parent_parent, _) = parents.last_mut().unwrap();

			if let Node::Done { .. } = &mut parent {
				match parent.get_children_mut().and_then(|children| children.last()) {
					Some(Node::Expression { .. }) |
					Some(Node::Return { .. })     |
					Some(Node::Do         { .. }) => { /* OK */ }
					_                       => {
						self.errors.push(node_error!(self, parent.get_row(), "A 'done' block must end in an expression or a 'do' statement"));
					}
				}
			}
			if let Node::Does { .. } = &mut parent {
				match parent.get_children_mut().and_then(|children| children.last()) {
					Some(Node::Expression { .. }) |
					Some(Node::Return { .. })     => { /* OK */ }
					_ => {
						self.errors.push(node_error!(self, parent.get_row(), "A 'does' block must end in an expression or return statement"));
					}
				}
			}
			
			if let Some(children) = parent_parent.get_children_mut() {
				children.push(parent);
			} else {
				self.errors.push(node_error!(self, parent.get_row(), "You can not start a block with an expression"));
			}
		}
	}

	fn expressionify_special(row: usize, mut tokens: Vec<Token>) -> Node {
		if tokens[0].token_type == Type::A {
			return Node::AExpression { row: row, expression: tokens }
		}
		// Left-most 'of' determines place where to split the expression
		let opt_idx = tokens.iter().position(|tok| tok.token_type == Type::Of);
		if let Some(idx) = opt_idx {
			let right = tokens.split_off(idx);
			return Node::OfExpression { row: row, left: tokens, right: right }
		}
		Self::expressionify(row, tokens)
	}

	fn expressionify(row: usize, tokens: Vec<Token>) -> Node {
		Node::Expression{ row: row, expression: tokens }
	}

	fn childrenify_expressions<'a>(number: usize, expressions: RowExpressions<'a>, expressionifier: fn(usize, Vec<Token<'a>>) -> Node<'a>) -> Vec<Node<'a>> {
		let mut children = vec![];
		if expressions.expression.len() > 0 {
			children.push(expressionifier(number, expressions.expression));
		}
		if expressions.block_expression.len() > 0 {
			children.push(expressionifier(number, expressions.block_expression));
		}
		children
	}
}

#[cfg(test)]
use crate::tokenizing::lexer::Lexer;
#[cfg(test)]
use crate::parsing::row_parser::parse as row_parse;

macro_rules! stringify_node {
	($name:expr, $depth:expr) => {
		format!("{}{}", ".".repeat($depth), $name)
	};
}

macro_rules! stringify_children {
	($name:expr, $depth:expr, $nodes:ident) => {
		format!("{}{}{}", ".".repeat($depth), $name, $nodes.into_iter().map(|child| debug_stringify_tree(child, $depth + 1)).collect::<String>() )
	};
}

pub fn debug_stringify_tree(node: &Node, depth: usize) -> String {
	use self::Node as N;
	match node {
		N::Root      { children }     => stringify_children!("root", depth, children),
		N::Is        { children, .. } => stringify_children!("is", depth, children),
		N::Does      { children, .. } => stringify_children!("does", depth, children),
		N::Has       { children, .. } => stringify_children!("has", depth, children),
		N::Done      { children, .. } => stringify_children!("done", depth, children),
		N::Use       { children, .. } => stringify_children!("use", depth, children),
		N::Uses      { children, .. } => stringify_children!("uses", depth, children),
		N::Return    { children, .. } => stringify_children!("res", depth, children),
		N::Erroneous { children, .. } => stringify_children!("err!", depth, children),
		N::Do {
			expression: None,
			children,
			.. 
		} => stringify_children!("do", depth, children),
		N::Do {
			expression: Some(boxed_expression),
			children,
			..
		} => stringify_children!(format!("do{}", debug_stringify_tree(boxed_expression, depth + 1)), depth, children),
		N::Expression    { .. } => stringify_node!("X", depth),
		N::AExpression   { .. } => stringify_node!("aX", depth),
		N::OfExpression  { .. } => stringify_node!("ofX", depth),
	}
}

#[test]
fn test_faulty_expression_block() {
	let tokens = Lexer::tokenize_cookiean("
this is ok:
	\"Hello\"

this:
	ain't
").0;
	let rows = row_parse(tokens).0;
	let err = parse(rows).1;
	assert!(err.is_none(), "Unexpected error {:?}", err);
}

#[test]
fn test_return_at_the_end_of_does() {
	let tokens = Lexer::tokenize_cookiean("
x does:
	return nothing
").0;
	let rows = row_parse(tokens).0;
	let err = parse(rows).1;
	assert!(err.is_none(), "Unexpected error {:?}", err);
}

#[test]
fn test_missing_block() {
	let tokens = Lexer::tokenize_cookiean("
x is q:
").0;
	let rows = row_parse(tokens).0;
	let err = parse(rows).1;
	assert!(err.is_some(), "Expected error");
	assert_eq!(err.unwrap().errors.len(), 1);
}

#[test]
fn partial_test_parser_all_nodes_1() {
	let tokens = Lexer::tokenize_cookiean("
use standard context

this is a test
[x] squared is x * x
printed does displayed
[x] twice does:
	x then x
do \"Hello [name]!\" displayed

").0;
	let rows = row_parse(tokens).0;
	let root = parse(rows).0;
	assert_eq!(debug_stringify_tree(&root, 0), "
		root
		.use..X
		.is..X
		.is..X
		.does..X
		.does
	..X
		.do..X".chars().filter(|&c| !" \t\n".contains(c)).collect::<String>()
	);
}

#[test]
fn partial_test_parser_all_nodes_2() {
	let tokens = Lexer::tokenize_cookiean("
new context uses func of package:
	pi is 3

a name has :
	first as a string
	a second
	length of itself

a person has a name\n").0;
	let rows = row_parse(tokens).0;
	let root = parse(rows).0;
	assert_eq!(debug_stringify_tree(&root, 0), "
		root
		.uses..ofX
		..is...X
		.has
		..X
		..aX
		..ofX
		.has
		..aX".chars().filter(|&c| !" \t\n".contains(c)).collect::<String>()
	);
}

#[test]
fn test_label() {
	let tokens = Lexer::tokenize_cookiean("
\"label\" :
\tthis is america\n").0;
	let rows = row_parse(tokens).0;
	let (root, errors) = parse(rows);
	assert!(errors.is_none(), "Expected no block parsing errors, but got: {:?}", errors);
	assert_eq!(debug_stringify_tree(&root, 0), "
		root
		.err!
		..is...X".chars().filter(|&c| !" \t\n".contains(c)).collect::<String>()
	);
}

#[test]
fn partial_test_parser_all_nodes_3() {
	let tokens = Lexer::tokenize_cookiean("
Waffe is a person:
	age is: 23
	name is:
		first is \"Walter\"
		second is \"Grönholm\"

[input] node has :
	value is input

[f'd] done twice: f'd then f'd\n").0;
	let rows = row_parse(tokens).0;
	let root = parse(rows).0;
	assert_eq!(debug_stringify_tree(&root, 0), "
		root
		.is..X
		..is...X
		..is
		...is....X
		...is....X
		.has
		..is...X
		.done..X".chars().filter(|&c| !" \t\n".contains(c)).collect::<String>()
	);
}

#[test]
fn partial_test_parser_done_block() {
	let tokens = Lexer::tokenize_cookiean("[f] done timed:\n\ttime is 1000\n\tf\n").0;
	let rows = row_parse(tokens).0;
	let (root, err) = parse(rows);
	assert_eq!(debug_stringify_tree(&root, 0), String::from("root.done..is...X..X"));
	assert!(err.is_none(), "Unexpected error message: {}", err.unwrap());
}

#[test]
fn partial_test_parser_indentation_error_1() {
	let tokens = Lexer::tokenize_cookiean("x is:\n\t\t1\n").0;
	let rows = row_parse(tokens).0;
	let err = parse(rows).1.unwrap();
	assert!(format!("{}", err).contains("rror at row 2"), "Could not find 'error at row 2' in error message: {}", err);
	assert!(err.errors.len() >= 1, "Expected at least 1 error");
}

#[test]
fn partial_test_parser_indentation_error_2() {
	let tokens = Lexer::tokenize_cookiean("this is:\n\"hello\"\n\t1\n").0;
	let rows = row_parse(tokens).0;
	let err = parse(rows).1.unwrap();
	assert!(format!("{}", err).contains("rror at row 2"), "Could not find 'error at row 2' in error message: {}", err);
	assert!(format!("{}", err).contains("rror at row 3"), "Could not find 'error at row 3' in error message: {}", err);
	assert!(err.errors.len() >= 2, "Expected at least 2 errors");
}

#[test]
fn partial_test_parser_verb_block_errors_ok() {
	let tokens = Lexer::tokenize_cookiean("
context uses:
	second uses:
		things
	x is 10
	f does g
	t has a y
do:
	x is 10
	f does g
	t has a y
t1 has:
	t2 has a stuff
").0;
	let rows = row_parse(tokens).0;
	let err = parse(rows).1;
	assert!(err.is_none(), "Got an error: {:?}", err);
}

#[test]
fn test_parser_basics() {
	let tokens = Lexer::tokenize_cookiean("hi my name is what:\n\t[my] name [me] does : who\n").0;
	let rows = row_parse(tokens).0;
	use self::Node as N;
	assert_eq!(
		parse(rows).0,
		N::Root {
			children: vec![
				N::Is {
					row: 1, 
					data: StatementRowStructure {
						parameter: None,
						variable_name: vec![Token { token_type: Type::Word, string: "hi", column: 3 },
						                    Token { token_type: Type::Word, string: "my", column: 6 },
						                    Token { token_type: Type::Word, string: "name", column: 11 }],
						self_reference: None,
					},
					children: vec![
						N::Expression {
							row: 1,
							expression: vec![Token { token_type: Type::Word, string: "what", column: 19 }]
						},
						N::Does {
							row: 2, 
							data: StatementRowStructure {
								parameter: Some(vec![Token { token_type: Type::Word, string: "my", column: 5 }]),
								variable_name: vec![Token { token_type: Type::Word, string: "name", column: 11 }],
								self_reference: Some(vec![Token { token_type: Type::Word, string: "me", column: 15 }]),	
							},
							children: vec![N::Expression { row: 2, expression: vec![Token { token_type: Type::Word, string: "who", column: 27 }] }]
						}
					]
				}
			]
		}
	)
}