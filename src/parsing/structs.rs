
use crate::tokenizing::structs::{Token, Type};

#[derive(PartialEq, Debug)]
/// Row structure for statements
/// 
/// In the form of:
/// [parameter] variable_name [self_reference] 'is'/'does'/'uses'/'has' expression block_starter block_expression
pub struct StatementRowStructure<'a> {
	pub parameter: Option<Vec<Token<'a>>>,
	/// Must not be empty
	pub variable_name: Vec<Token<'a>>,
	pub self_reference: Option<Vec<Token<'a>>>,
}

#[derive(PartialEq, Debug)]
/// Row structure for (verbless) expressions
/// 
/// E.g. "Hello world!" displayed
pub struct ExpressionRowStructure<'a> {
	pub expression: Vec<Token<'a>>,
}

#[derive(PartialEq, Debug)]
/// Row structure specifically for Done statements
/// 
/// In the form of:
/// [parameter] done variable_name [self_reference]: block_expression
pub struct DoneRowStructure<'a> {
	pub parameter: Vec<Token<'a>>,
	pub variable_name: Vec<Token<'a>>,
	pub self_reference: Option<Vec<Token<'a>>>,
	pub block_expression: Vec<Token<'a>>
}

#[derive(PartialEq, Debug)]
pub struct RowExpressions<'a> {
	/// Both expression and block_starter may not be empty
	pub expression: Vec<Token<'a>>,
	/// Both expression and block_starter may not be empty
	pub block_starter: Option<Type>,
	/// If block_expression is not empty then block_starter may not be empty
	pub block_expression: Vec<Token<'a>>
}

#[derive(PartialEq, Debug)]
pub enum RowStructure<'a> {
	IsStatement(StatementRowStructure<'a>, RowExpressions<'a>),
	DoesStatement(StatementRowStructure<'a>, RowExpressions<'a>),
	HasStatement(StatementRowStructure<'a>, RowExpressions<'a>),
	UsesStatement(StatementRowStructure<'a>, RowExpressions<'a>),
	DoCommand(RowExpressions<'a>),
	UseCommand(RowExpressions<'a>),
	ReturnCommand(RowExpressions<'a>),
	Done(DoneRowStructure<'a>),
	Expression(ExpressionRowStructure<'a>),
	ErrorBlock
}


#[derive(PartialEq, Debug)]
pub enum Node<'a> {
	// For file level only
	Root         {children: Vec<Node<'a>>},
	// x is 10
	// [n] factorial is: ...
	// name of [me] is: ...
	// [other] x [me] is: ...
	Is           {row: usize, data: StatementRowStructure<'a>, children: Vec<Node<'a>>},
	// print does "Hello" displayed
	// print [me] does me displayed
	// [html] parsed does: ...
	// [html] parsed by [me] does: ...
	Does         {row: usize, data: StatementRowStructure<'a>, children: Vec<Node<'a>>},
	// a person has: ...
	// a child of [me] has: ...
	// [type] list has: ...
	// [color] colored [car] has: ...
	Has          {row: usize, data: StatementRowStructure<'a>, children: Vec<Node<'a>>},
	// do: ...
	// do if x = 0: ...
	Do           {row: usize, expression: Option<Box<Node<'a>>>, children: Vec<Node<'a>>},
	// [f'd] done twice: f'd then f'd
	// [f'd] done if [x]: ...
	Done         {row: usize, function_parameter: Vec<Token<'a>>, right: Vec<Token<'a>>, this: Option<Vec<Token<'a>>>, children: Vec<Node<'a>>},
	// Waffe uses last name of Peter
	Uses         {row: usize, data: StatementRowStructure<'a>, children: Vec<Node<'a>>},
	// use math package
	Use          {row: usize, children: Vec<Node<'a>>},
	// result in 0
	Return     {row: usize, expression: Option<Box<Node<'a>>>, children: Vec<Node<'a>>},
	/// Expressions:
	/// n * n
	/// the first name of person
	///
	/// Note: special meaning when inside 'has', 'use' and 'uses' blocks:
	/// input --> input is/uses input
	Expression   {row: usize, expression: Vec<Token<'a>>},
	/// Special expression in 'has', 'use' and 'uses' blocks.
	/// 
	/// ip of server --> ip is ip server
	OfExpression {row: usize, left: Vec<Token<'a>>, right: Vec<Token<'a>>},
	/// Special expression in 'has', 'use' and 'uses' blocks.
	/// 
	/// a name    --> name is/uses a name
	/// an age    --> age is/uses an age
	/// some data --> data is/uses some data
	AExpression  {row: usize, expression: Vec<Token<'a>>},
	/// Used only to allow block parsing with erroneous rows
	Erroneous    {row: usize, children: Vec<Node<'a>>},
}
impl Node<'_> {
	pub fn get_row(&self) -> usize {
		use self::Node as N;
		match self {
			N::Root { .. } => 0,
			N::Is           { row, .. } |
			N::Does         { row, .. } |
			N::Has          { row, .. } |
			N::Do           { row, .. } |
			N::Done         { row, .. } |
			N::Uses         { row, .. } |
			N::Use          { row, .. } |
			N::Return       { row, .. } |
			N::Erroneous    { row, .. } |
			N::OfExpression { row, .. } |
			N::AExpression  { row, .. } |
			N::Expression   { row, .. } => {
				*row
			}
		}
	}
	pub fn get_children_mut(&mut self) -> Option<&mut Vec<Self>> {
		use self::Node as N;
		match self {
			N::Root       { children }     |
			N::Is         { children, .. } |
			N::Does       { children, .. } |
			N::Has        { children, .. } |
			N::Do         { children, .. } |
			N::Done       { children, .. } |
			N::Uses       { children, .. } |
			N::Use        { children, .. } |
			N::Return     { children, .. } |
			N::Erroneous  { children, .. } => Some(children),
			N::OfExpression      { .. } |
			N::AExpression       { .. } |
			N::Expression        { .. } => {
				None
			}
		}
	}
}