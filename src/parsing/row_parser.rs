
use std::fmt;
use crate::tokenizing::structs::{Token, Type};
use crate::tokenizing::structs::{Errors, Error};
use crate::parsing::structs::*;

#[derive(Debug, PartialEq)]
pub struct Row<'a> {
	pub number: usize,
	pub indentation: usize,
	pub structure: RowStructure<'a>
}
impl fmt::Display for Row<'_> {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "#{}\t{:#?}", self.number, self.structure)
	}
}

trait RowGetter {
	fn get_row<'a, I>(&mut self, tokens: &mut I) -> Option<Row<'a>>
		where I: Iterator<Item = Token<'a>>;
}

macro_rules! push_error {
	($parser:ident, $col:expr, $($arg:tt)*) => (
		$parser.errors.push(
			Error { error_type: "Row parsing".to_owned(), message: format!($($arg)*), row: $parser.row + 1, column: $col }
		);
	)
}

macro_rules! handle_row_error {
	($parser:ident, $token:ident, $tokens:ident, $indent:ident, $($arg:tt)*) => (
		{
			// Push error
			$parser.errors.push(
				Error { error_type: "Row parsing".to_owned(), message: format!($($arg)*), row: $parser.row + 1, column:  $token.column }
			);
			// TODO detail error with some code
			// Parse until newline
			let mut block_starter = $token.token_type == Type::Colon;
			if $token.token_type != Type::NewLine {
				while let Some(tok) = $tokens.next() {
					if tok.token_type == Type::Colon { block_starter = true; }
					if tok.token_type == Type::NewLine {
						$parser.row += 1;
						break;
					}
				}
			}
			// Return row if it would help with block parsing
			return if block_starter {
				Some(Row { number: $parser.row, indentation: $indent, structure: RowStructure::ErrorBlock })
			} else {
				None
			}
		}
	)
}

pub fn parse<'a>(tokens: Vec<Token<'a>>) -> (Vec<Row<'a>>, Option<Errors>) {
	RowParser::parse(tokens)
}

pub struct RowParser {}

impl RowParser {

	/// Verifies that each row follows one of the following structures:
	/// indentation expression
	/// indentation verb expression block_starter block_expression
	/// indentation [parameter] 'done' variable_name [self_reference]: block_expression
	/// indentation [parameter] variable_name [self_reference] verb expression block_starter block_expression
	/// 
	/// Any further checks are done in block_parser
	fn parse<'a, T>(tokens: T) -> (Vec<Row<'a>>, Option<Errors>) where T: IntoIterator<Item = Token<'a>> {
		let mut errors = Vec::new();

		let tokens = RowParser::strip_subsequent_separators(tokens, &mut errors);
		
		let rows = {
			RowParser::run(tokens, &mut RowGetterWithFSM {
				row: 0,
				errors: &mut errors
			})
		};
		if errors.is_empty() {
			(rows, None)
		} else {
			(rows, Some(Errors {
				message: format!("Found {} error(s) while parsing", errors.len()),
				errors: errors
			}))
		}
	}

	fn strip_subsequent_separators<'a, T>(tokens: T, errors: &mut Vec<Error>) -> Vec<Token<'a>> where T: IntoIterator<Item = Token<'a>> {
		let mut result = Vec::new();
		
		let mut previous_type: Type = Type::NonTabWhitespace;
		let mut row = 1;

		for token in tokens.into_iter() {
			if token.token_type == Type::NewLine {
				row += 1;
			}
			if (previous_type.is_separator() || previous_type == Type::Colon)
			&& (token.token_type.is_separator() || token.token_type == Type::Colon) {
				errors.push (Error {
					error_type: "Row parsing".to_owned(),
					message: format!("Repeated separators are not allowed. A {:?} may not be directly followed by a {:?}", previous_type, token.token_type),
					row: row,
					column: token.column
				});
			}
			previous_type = token.token_type;
			result.push(token);
		}
		return result;
	}

	fn run<'a, T, R>(tokens: T, getter: &mut R) -> Vec<Row<'a>> where T: IntoIterator<Item = Token<'a>>, R: RowGetter {
		let mut rows = Vec::new();
		let mut tokens = tokens.into_iter().peekable();
		while tokens.peek().is_some() {
			if let Some(row) = getter.get_row(&mut tokens) {
				rows.push(row);
			}
		}
		return rows;
	}
}

#[derive(Debug)]
enum ExpressionsFSMState {
	Entry,
	Expr,
	Colon
}

#[derive(Debug)]
enum DoneFSMState {
	DoneEntry,
	DoneExpr,
	DoneRef,
	DoneColon,
}

#[derive(Debug)]
enum FSMState<'a> {
	Entry,
	Param(Vec<Token<'a>>),
	ExprEntry(Vec<Token<'a>>),
	ParamExpr(StatementRowStructure<'a>),
	ExprRef(StatementRowStructure<'a>)
}

/// RowGetter v2, now with a hacky finite state machine
struct RowGetterWithFSM<'a> {
	row: usize,
	errors: &'a mut Vec<Error>
}
impl RowGetter for RowGetterWithFSM<'_>
{
	fn get_row<'a, I>(&mut self, tokens: &mut I) -> Option<Row<'a>>
	where I: Iterator<Item = Token<'a>> {
		let mut indentation = 0;
		let mut state = FSMState::Entry;
		let mut vertical_bar = false;
		while let Some(token) = tokens.next() {
			use self::FSMState as S;
			use self::Type as T;
			if token.token_type == T::NewLine {
				self.row += 1;
			}
			state = match (state, token.token_type) {
				(S::Entry, T::TabsMixedWhitespace) => {
					// Parse until newline
					let mut block_starter = false;
					let mut non_empty = false;
					while let Some(tok) = tokens.next() {
						if tok.token_type == T::NewLine {
							self.row += 1;
							break;
						}
						if tok.token_type == T::VerticalBar && !vertical_bar {
							vertical_bar = true;
							continue;
						}
						if tok.token_type == T::NonTabWhitespace
						|| tok.token_type == T::Tabs
						|| tok.token_type == T::TabsMixedWhitespace {
							continue
						}
						non_empty = true;
						if tok.token_type == T::Colon {
							block_starter = true;
						}
					}
					// Push error only if non empty row
					if non_empty {
						self.errors.push(
							Error {
								error_type: "Row parsing".to_owned(),
								message: "You have some invisible characters mixed in with your indentation. Tabs must be separated from other invisible characters using a vertical bar |".to_owned(),
								row: self.row + 1,
								column: token.column
							}
						);
					}
					// Return row if it would help with block parsing
					return if block_starter {
						Some(Row {
							number: self.row,
							indentation: token.string.matches('\t').count(), // Close enough
							structure: RowStructure::ErrorBlock
						})
					} else {
						None
					}
				},
				(S::Entry, T::Tabs) if !vertical_bar => {
					indentation += token.string.len();
					S::Entry
				},
				(S::Entry, T::VerticalBar) if !vertical_bar => {
					vertical_bar = true;
					S::Entry
				},
				(S::Entry, T::Tabs) if vertical_bar => {
					handle_row_error!(self, token, tokens, indentation,
						"Tabs may not follow a vertical bar |")
				},
				(S::Entry, T::VerticalBar) if vertical_bar => {
					handle_row_error!(self, token, tokens, indentation,
						"Only one vertical bar | is allow at the start of a line")
				},
				(S::Entry, T::Do) => {
					let expressions = self.parse_expressions(tokens, T::Do);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::DoCommand(expressions) })
				}
				(S::Entry, T::Use) => {
					let expressions = self.parse_expressions(tokens, T::Use);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::UseCommand(expressions) })
				}
				(S::Entry, T::BracketOpen) => {
					S::Param(self.parse_parameter(tokens))
				}
				(S::Entry, T::NewLine) => {
					return None
				}
				(S::Entry, T::Return) => {
					let expressions = self.parse_expressions(tokens, T::Return);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::ReturnCommand(expressions) })
				}
				(S::Entry, token_type) if token_type.is_expr() && !token_type.is_separator() => {
					S::ExprEntry(vec![token])
				}
				(S::Entry, _) => {
					handle_row_error!(self, token, tokens, indentation,
						"A row can't start with '{}'", token.string)
				}
				(S::Param(parameter), T::Done) => {
					let done = self.parse_done(tokens, parameter);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::Done(done) })
				}
				(S::Param(parameter), token_type) if token_type.is_expr() => {
					S::ParamExpr(StatementRowStructure {
						parameter: Some(parameter),
						variable_name: vec![token],
						self_reference: None
					})
				}
				(S::Param(_), token_type) if token_type.is_verb() => {
					handle_row_error!(self, token, tokens, indentation,
						"Missing variable name between parameter [..] and '{}'", token.string)
				}
				(S::Param(_), _) => {
					handle_row_error!(self, token, tokens, indentation,
						"After a parameter, you can only have a variable name or 'done'. Found: '{}'", token.string)
				}
				(S::ExprEntry(expression), T::BracketOpen) => {
					S::ExprRef(StatementRowStructure {
						parameter: None,
						variable_name: expression,
						self_reference: Some(self.parse_parameter(tokens))
					})
				}
				(S::ExprEntry(expression), T::NewLine) => {
					let structure = ExpressionRowStructure { expression: expression	};
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::Expression(structure)})
				}
				(S::ExprEntry(expression), T::Is) => {
					let structure = StatementRowStructure { parameter: None, variable_name: expression, self_reference: None };
					let expressions = self.parse_expressions(tokens, T::Is);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::IsStatement(structure, expressions) })
				}
				(S::ExprEntry(expression), T::Does) => {
					let structure = StatementRowStructure { parameter: None, variable_name: expression, self_reference: None };
					let expressions = self.parse_expressions(tokens, T::Does);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::DoesStatement(structure, expressions) })
				}
				(S::ExprEntry(expression), T::Has) => {
					let structure = StatementRowStructure { parameter: None, variable_name: expression, self_reference: None };
					let expressions = self.parse_expressions(tokens, T::Has);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::HasStatement(structure, expressions) })
				}
				(S::ExprEntry(expression), T::Uses) => {
					let structure = StatementRowStructure { parameter: None, variable_name: expression, self_reference: None };
					let expressions = self.parse_expressions(tokens, T::Uses);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::UsesStatement(structure, expressions) })
				}
				(S::ExprEntry(_), T::Use) => {
					handle_row_error!(self, token, tokens, indentation,
						"Found 'use' after expression (or variable name). The command 'use' can only be used at the start of the row.")
				}
				(S::ExprEntry(_), T::Do) => {
					handle_row_error!(self, token, tokens, indentation,
						"Found 'do' after expression (or variable name). The command 'do' can only be used at the start of the row.")
				}
				(S::ExprEntry(mut expression), token_type) if token_type.is_expr() => {
					expression.push(token);
					S::ExprEntry(expression)
				}
				(S::ExprEntry(_), _) => {
					handle_row_error!(self, token, tokens, indentation,
						"Expression (or variable name) followed by unexpected '{}' token.\nExpected a parameter, variable name part, a verb (e.g. 'is') or a new line.", token.string)
				}
				(S::ParamExpr(mut structure), T::BracketOpen) => {
					structure.self_reference = Some(self.parse_parameter(tokens));
					S::ExprRef(structure)
				}
				(S::ExprRef(structure), T::Is)   |
				(S::ParamExpr(structure), T::Is) => {
					let expressions = self.parse_expressions(tokens, T::Is);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::IsStatement(structure, expressions) })
				}
				(S::ExprRef(structure), T::Does)   |
				(S::ParamExpr(structure), T::Does) => {
					let expressions = self.parse_expressions(tokens, T::Does);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::DoesStatement(structure, expressions) })
				}
				(S::ExprRef(structure), T::Has)   |
				(S::ParamExpr(structure), T::Has) => {
					let expressions = self.parse_expressions(tokens, T::Has);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::HasStatement(structure, expressions) })
				}
				(S::ExprRef(structure), T::Uses)   |
				(S::ParamExpr(structure), T::Uses) => {
					let expressions = self.parse_expressions(tokens, T::Uses);
					return Some(Row { number: self.row, indentation: indentation,
					                  structure: RowStructure::UsesStatement(structure, expressions) })
				}
				(S::ExprRef(_), _)   => {
					handle_row_error!(self, token, tokens, indentation,
						"Self reference parameter [..] followed by unexpected '{}' token.\nExpected 'is', 'does', 'has' or 'uses' verb.", token.string)
				}
				(S::ParamExpr(mut structure), token_type) if token_type.is_expr() => {
					structure.variable_name.push(token);
					S::ParamExpr(structure)
				}
				(S::ParamExpr(_), _) => {
					handle_row_error!(self, token, tokens, indentation,
						"Statement followed by unexpected '{}' token.\nExpected a parameter, variable name part, a verb (e.g. 'does') or a new line.", token.string)
				}
			}
		}
		None
	}
}
impl RowGetterWithFSM<'_> {
	fn parse_parameter<'a, I>(&mut self, tokens: &mut I) -> Vec<Token<'a>>
	where I: Iterator<Item = Token<'a>> {
		let mut parameter = Vec::new();
		while let Some(token) = tokens.next() {
			
			if token.token_type == Type::NewLine {
				self.row += 1;
			}
			match token.token_type {
				Type::BracketClose => {
					if parameter.is_empty() {
						push_error!(self, token.column,
							"A parameter [..] must contain at least one word.");
					}
					break;
				}
				Type::A    |
				Type::The  |
				Type::Word => {
					parameter.push(token)
				}
				Type::NewLine => {
					push_error!(self, token.column,
						"Incomplete parameter and row");
					break
				}
				token_type if token_type.is_separator() => {
					push_error!(self, token.column,
						"A parameter [...] may not contain commas or semicolons. Found: '{}'", token.string);
				}
				_ => {
					push_error!(self, token.column,
						"A parameter [...] may not contain special words. Found: '{}'", token.string);
				}
			}
		}
		parameter
	}

	fn parse_expressions<'a, I>(&mut self, tokens: &mut I, entered_from: Type) -> RowExpressions<'a>
	where I: Iterator<Item = Token<'a>> {

		let mut state = ExpressionsFSMState::Entry;
		let mut expressions = RowExpressions {
			expression: Vec::new(),
			block_starter: None,
			block_expression: Vec::new()
		};
		while let Some(token) = tokens.next() {
			use self::ExpressionsFSMState as S;
			use crate::tokenizing::structs::Type as T;

			if token.token_type == T::NewLine {
				self.row += 1;
			}
			state = match (state, token.token_type) {
				(S::Expr, T::Colon)  |
				(S::Entry, T::Colon) => {
					expressions.block_starter = Some(T::Colon);
					S::Colon
				}
				(S::Expr, T::NewLine) |
				(S::Colon, T::NewLine) => {
					break
				}
				(S::Expr, token_type)  |
				(S::Entry, token_type) if token_type.is_expr() => {
					expressions.expression.push(token);
					S::Expr
				}
				(S::Colon, token_type) if token_type.is_expr() => {
					expressions.block_expression.push(token);
					S::Colon
				}
				(S::Entry, T::NewLine) => {
					if entered_from == T::Return {
						/* Ok to have empty return */
					} else {
						push_error!(self, token.column,
							"Missing expression or a colon after {:?}", entered_from);
					}
					break
				}
				(state, token_type) => {
					// TODO do error handling
					push_error!(self, token.column,
						"Got {:?} when in {:?} state", token_type, state);
					break
				}
			}
		}
		expressions
	}

	fn parse_done<'a, I>(&mut self, tokens: &mut I, parameter: Vec<Token<'a>>) -> DoneRowStructure<'a>
	where I: Iterator<Item = Token<'a>> {

		let mut state = DoneFSMState::DoneEntry;
		let mut done = DoneRowStructure {
			parameter: parameter,
			variable_name: Vec::new(),
			self_reference: None,
			block_expression: Vec::new()
		};
		while let Some(token) = tokens.next() {
			use self::DoneFSMState as S;
			use crate::tokenizing::structs::Type as T;
			
			if token.token_type == T::NewLine {
				self.row += 1;
			}
			state = match (state, token.token_type) {
				(S::DoneExpr, T::BracketOpen) => {
					done.self_reference = Some(self.parse_parameter(tokens));
					S::DoneRef
				}
				(S::DoneRef, T::Colon)  |
				(S::DoneExpr, T::Colon) => {
					S::DoneColon
				}
				(S::DoneExpr, _) |
				(S::DoneEntry, _) if token.token_type.is_expr() => {
					done.variable_name.push(token);
					S::DoneExpr
				}
				(S::DoneColon, T::NewLine) => {
					break
				}
				(S::DoneColon, _) if token.token_type.is_expr() => {
					done.block_expression.push(token);
					S::DoneColon
				}
				(state, token_type) => {
					// TODO do error handling
					push_error!(self, token.column,
						"Got {:?} when in {:?} state", token_type, state);
					break
				}
			}
		}
		done
	}
}

#[cfg(test)]
use crate::tokenizing::lexer::Lexer;

#[test]
fn test_row_parser_empty_rows() {
	let tokens = Lexer::tokenize_cookiean("\n\n").0;
	let (rows, errors) = parse(tokens);
	assert!(errors.is_none(), "Found unexpected errors: {:?}", errors);
	assert_eq!(rows, vec![])
}

#[test]
fn test_row_parser_parameter() {
	let (rows, errors) = parse(Lexer::tokenize_cookiean("[X] Y is Z\n").0);
	assert!(errors.is_none(), "Found unexpected errors: {:?}", errors);
	assert_eq!(
		rows,
		vec![Row {
			number: 1,
			indentation: 0,
			structure: RowStructure::IsStatement(StatementRowStructure {
				parameter: Some(vec![
					Token { string: "X", token_type: Type::Word, column: 3}
				]),
				variable_name: vec![
					Token { string: "Y", token_type: Type::Word, column: 6}
				],
				self_reference: None,}, RowExpressions {
				expression: vec![
					Token { string: "Z", token_type: Type::Word, column: 11}
				],
				block_starter: None,
				block_expression: vec![]
			})
		}])
}

#[test]
fn test_row_parser_empty_parameter() {
	let (result, errors) = parse(Lexer::tokenize_cookiean("[] generated does:\n").0);
	assert!(errors.is_some(), "Expected errors but got result {:#?}", result);
	let errors = errors.unwrap().errors;
	assert!(errors.len() == 1, "Expected 1 error but found {}", errors.len())
}

#[test]
fn test_row_parser_done() {
	let (rows, errors) = parse(Lexer::tokenize_cookiean("[f'd] done ly [me]:\n").0);
	assert!(errors.is_none(), "Found unexpected errors: {:?}", errors);
	assert_eq!(
		rows,
		vec![Row {
			number: 1,
			indentation: 0,
			structure: RowStructure::Done(DoneRowStructure {
				parameter: vec![
					Token { string: "f'd", token_type: Type::Word, column: 5}
				],
				variable_name: vec![
					Token { string: "ly", token_type: Type::Word, column: 14}
				],
				self_reference: Some(vec![
					Token { string: "me", token_type: Type::Word, column: 18}
				]),
				block_expression: vec![],
			})
		}])
}

#[test]
fn test_row_parser_reference() {
	let (rows, errors) = parse(Lexer::tokenize_cookiean("X [Y] is Z\n").0);
	assert!(errors.is_none(), "Found unexpected errors: {:?}", errors);
	assert_eq!(
		rows,
		vec![Row {
			number: 1,
			indentation: 0,
			structure: RowStructure::IsStatement(StatementRowStructure {
				parameter: None,
				variable_name: vec![
					Token { string: "X", token_type: Type::Word, column: 2}
				],
				self_reference: Some(vec![
					Token { string: "Y", token_type: Type::Word, column: 5}
				]),}, RowExpressions {
				expression: vec![
					Token { string: "Z", token_type: Type::Word, column: 11}
				],
				block_starter: None,
				block_expression: vec![]
			})
		}]
	)
}

#[test]
fn test_row_indentation_bar_1() {
	let (rows, errors) = parse(Lexer::tokenize_cookiean("\t\t|  heyo\n").0);
	assert!(errors.is_none(), "Found unexpected errors: {:?}", errors);
	assert_eq!(
		rows,
		vec![Row {
			number: 1,
			indentation: 2,
			structure: RowStructure::Expression(ExpressionRowStructure {
				expression: vec![
					Token { string: "heyo", token_type: Type::Word, column: 10}
				]
			})
		}]
	)
}

#[test]
fn test_row_indentation_bar_2() {
	let (_, errors) = parse(Lexer::tokenize_cookiean("\t\t  nuh uh\n").0);
	assert!(errors.is_some(), "The lack of a vertical bar between tabs and spaces should produce an error");
}

#[test]
fn test_row_indentation_bar_3() {
	let (rows, errors) = parse(Lexer::tokenize_cookiean("|  heyo\n").0);
	assert!(errors.is_none(), "Found unexpected errors: {:?}", errors);
	assert_eq!(
		rows,
		vec![Row {
			number: 1,
			indentation: 0,
			structure: RowStructure::Expression(ExpressionRowStructure {
				expression: vec![
					Token { string: "heyo", token_type: Type::Word, column: 8}
				]
			})
		}]
	)
}

#[test]
fn test_empty_row_mixed_whitespace() {
	let (_, errors) = parse(Lexer::tokenize_cookiean("\t\t\r\n\t\t \n").0);
	assert!(errors.is_none(), "Found unexpected errors: {:?}", errors);
}


#[test]
fn row_parser_test_abrupt_row() {
	let (result, errors) = parse(Lexer::tokenize_cookiean("x is\n").0);
	assert!(errors.is_some(), "Expected errors but got result {:#?}", result);
	let errors = errors.unwrap().errors;
	assert!(errors.len() == 1, "Expected 1 error but found {}", errors.len())
}


#[test]
fn row_parser_test_only_return() {
	let (_, errors) = parse(Lexer::tokenize_cookiean("return\n").0);
	assert!(errors.is_none(), "An sole return should be valid code. Got error: {:#?}", errors);
	// TODO result
}


#[test]
fn row_parser_test_only_do() {
	let (result, errors) = parse(Lexer::tokenize_cookiean("do\n").0);
	assert!(errors.is_some(), "Expected errors but got result {:#?}", result);
	// TODO result
}


#[test]
#[ignore]
fn row_parser_test_do_comma_word() {
	let (result, errors) = parse(Lexer::tokenize_cookiean("do, word").0);
	assert!(errors.is_some(), "Expected errors but got result {:#?}", result);
	let errors = errors.unwrap().errors;
	assert!(errors.len() == 1, "Expected 1 error but found {}", errors.len())
}


#[test]
fn row_parser_test_double_comma() {
	let (result, errors) = parse(Lexer::tokenize_cookiean("hello,,what\n").0);
	assert!(errors.is_some(), "Expected errors but got result {:#?}", result);
	let errors = errors.unwrap().errors;
	assert!(errors.len() == 1, "Expected 1 error but found {}", errors.len())
}


#[test]
fn row_parser_test_separator_in_param() {
	let (result, errors) = parse(Lexer::tokenize_cookiean("[x,y] to distance does:\n").0);
	assert!(errors.is_some(), "Expected errors but got result {:#?}", result);
	let errors = errors.unwrap().errors;
	assert!(errors.len() == 1, "Expected 1 error but found {}", errors.len())
}


#[test]
fn row_parser_test_double_semicolon() {
	let (result, errors) = parse(Lexer::tokenize_cookiean("hello;;what\n").0);
	assert!(errors.is_some(), "Expected errors but got result {:#?}", result);
	let errors = errors.unwrap().errors;
	assert!(errors.len() == 1, "Expected 1 error but found {}", errors.len())
}


#[test]
fn row_parser_test_double_separator() {
	let (result, errors) = parse(Lexer::tokenize_cookiean("hello , ; what\n").0);
	assert!(errors.is_some(), "Expected errors but got result {:#?}", result);
	let errors = errors.unwrap().errors;
	assert!(errors.len() == 1, "Expected 1 error but found {}", errors.len())
}


#[test]
fn row_parser_test_empty_semicolon() {
	let (result, errors) = parse(Lexer::tokenize_cookiean(";\n").0);
	assert!(errors.is_some(), "Expected errors but got result {:#?}", result);
	let errors = errors.unwrap().errors;
	assert!(errors.len() == 1, "Expected 1 error but found {}", errors.len())
}


#[test]
fn row_parser_test_comma_after_statement() {
	let (_, errors) = parse(Lexer::tokenize_cookiean("123,\n").0);
	assert!(errors.is_none(), "Comma should be valid after a statement, even if nothing follows it. Got error: {:#?}", errors);
}


#[test]
fn row_parser_test_semicolon_after_statement() {
	let (_, errors) = parse(Lexer::tokenize_cookiean("hello, displayed;\n").0);
	assert!(errors.is_none(), "Semicolon should be valid after a statement, even if nothing follows it. Got error: {:#?}", errors);
}


#[test]
fn row_parser_test_comma_after_is() {
	let (_, errors) = parse(Lexer::tokenize_cookiean("green is, however, a natural color\n").0);
	assert!(errors.is_none(), "Comma should be valid after 'is'. Got error: {:#?}", errors);
}


#[test]
fn row_parser_test_double_comma_after_is() {
	let (result, errors) = parse(Lexer::tokenize_cookiean("green is cool,, right\n").0);
	assert!(errors.is_some(), "Expected errors but got result {:#?}", result);
	let errors = errors.unwrap().errors;
	assert!(errors.len() == 1, "Expected 1 error but found {}", errors.len())
}

// TODO more tests on faulty row structures

#[test]
fn test_row_parser_basics() {
	let (rows, errors) = parse(Lexer::tokenize_cookiean("hi my name is what:\n\n\t[my] name [me] is : who\n").0);
	assert!(errors.is_none(), "Found unexpected errors: {:?}", errors);
	assert_eq!(
		rows,
		vec![
			Row {
				number: 1,
				indentation: 0,
				structure: RowStructure::IsStatement(StatementRowStructure {
					parameter: None,
					variable_name: vec![
						Token { string: "hi", token_type: Type::Word, column: 3 },
						Token { string: "my", token_type: Type::Word, column: 6 },
						Token { string: "name", token_type: Type::Word, column: 11 }
					],
					self_reference: None,}, RowExpressions {
					expression: vec![
						Token { string: "what", token_type: Type::Word, column: 19 }
					],
					block_starter: Some(Type::Colon),
					block_expression: vec![]
				})
			},
			Row {
				number: 3,
				indentation: 1,
				structure: RowStructure::IsStatement(StatementRowStructure {
					parameter: Some(vec![
						Token { string: "my", token_type: Type::Word, column: 5 }
					]),
					variable_name: vec![
						Token { string: "name", token_type: Type::Word, column: 11 },
					],
					self_reference: Some(vec![
						Token { string: "me", token_type: Type::Word, column: 15 }
					]),}, RowExpressions {
					expression: vec![],
					block_starter: Some(Type::Colon),
					block_expression: vec![
						Token { string: "who", token_type: Type::Word, column: 25 }
					]
				})
			}
		]
	);
}